/*============================
 
 Pro Shot
 
 iOS 7/8 iPhone Photo Editor App template
 created by FV iMAGINATION - 2014
 http://www.fvimagination.com
 
==============================*/


#import <UIKit/UIKit.h>
#import "TOCropViewController.h"


@interface HomeVC : UIViewController <
UIImagePickerControllerDelegate,
UINavigationControllerDelegate,
TOCropViewControllerDelegate
>


// Logo image
@property (strong, nonatomic) IBOutlet UIImageView *logoImage;

// BUTTONS =============
@property (weak, nonatomic) IBOutlet UIButton *cameraOutlet;
@property (weak, nonatomic) IBOutlet UIButton *libraryOutlet;
@property (weak, nonatomic) IBOutlet UIButton *sampleImageOutlet;


// LABELS =========
@property (weak, nonatomic) IBOutlet UILabel *takeApicLabel;
@property (weak, nonatomic) IBOutlet UILabel *pickFromLibLabel;
@property (weak, nonatomic) IBOutlet UILabel *sampleImageLabel;

@property (strong, nonatomic) IBOutlet UIImageView *bkgImage;

@end

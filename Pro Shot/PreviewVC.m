/*============================
 
 Pro Shot
 
 iOS 7/8 iPhone Photo Editor App template
 created by FV iMAGINATION - 2014
 http://www.fvimagination.com
 
 ==============================*/


#import "UIImage+Utility.h"


#import "PreviewVC.h"
#import "ImageEditor.h"
#import "SharingVC.h"
#import "Configs.h"
#import "ReadNWrite.h"

#import <Social/Social.h>


@interface PreviewVC ()
<
ImageEditorDelegate,
ImageEditorTransitionDelegate,
ImageEditorThemeDelegate
>
@end


@implementation PreviewVC

// Hide the Status Bar
- (BOOL)prefersStatusBarHidden {
    return true;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    // Load the image based on the choice you made from IntroScreen
    _imageView.image = passedImage;

  //  [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isAdRemoved"];
    // Load IAP
    
   
    #pragma mark - DAILY LOCAL NOTIFICATION  ====================
    UILocalNotification *localNotification = [[UILocalNotification alloc]init];
    
    if (!localNotification)
        return;
    
    NSCalendar *calendar = [NSCalendar currentCalendar]; // gets default calendar
    NSDateComponents *components = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:[NSDate date]];
    
    /* IMPORTANT: SET HERE THE DAILY TIME YOU WANT THE LOCAL NOTIFICATION TO POPUP
    (change the time to some minutes forward your current time to test it,
     and remember that time is 24h formatted, so if you want a notification to popup
     after 12:00 AM, remember that: 13 = 1:00PM, 14 = 2:00 PM, and so on... */
    [components setHour:10];
    [components setMinute:00];
    
    localNotification.fireDate = [calendar dateFromComponents:components];
    // Set the fire date/time
    [localNotification setTimeZone:[NSTimeZone defaultTimeZone]];
   
    // SET THE MESSAGE OF YOUR NOTIFICATION
    [localNotification setAlertBody:@"Hi There, your friends are waiting to get new cool pictures from you!" ];
    
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    
/* END DAILY LOCAL NOTIFICATION ==================== */

    
    
    [self refreshImageView];
    
    
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    
   
   
    NSLog(@"self.buttonsView %@",self.buttonsView);
    //iapMade= [[NSUserDefaults standardUserDefaults] boolForKey:@"iapMade"];
    // NSLog(@"IAP MADE: %d", iapMade);
    NSMutableArray *pListArray = [[NSMutableArray alloc] init];
    pListArray = [ReadNWrite readFromDoucmentDirectory:@"userInfo.plist"];
    
    if(pListArray.count && (![[[pListArray objectAtIndex:0] objectForKey:@"iapMade"] isEqual:[NSNull null]]))
    {
        //NSLog(@"pListArray pListArray");
        iapMade= [[[pListArray objectAtIndex:0] objectForKey:@"iapMade"] boolValue];
    //    isAdRemoved= [[[pListArray objectAtIndex:0] objectForKey:@"isAdRemoved"] boolValue];
        
    }else
    {
        
        iapMade= [[NSUserDefaults standardUserDefaults] boolForKey:@"iapMade"];
//isAdRemoved= [[NSUserDefaults standardUserDefaults] boolForKey:@"isAdRemoved"];
        
        
    }
    
    isAdRemoved=[[NSUserDefaults standardUserDefaults] boolForKey:@"isAdRemoved"];
    
    
    if(!isAdRemoved)
    {
        
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
        {
//            edited by Monir
//            self.buttonsView.frame=CGRectMake(0, [UIScreen mainScreen].bounds.size.height-80-55 , [UIScreen mainScreen].bounds.size.width , 80+55);
            self.buttonsView.frame=CGRectMake(0, [UIScreen mainScreen].bounds.size.height-90-54 , [UIScreen mainScreen].bounds.size.width , 90+54);
//
        }
        else
        {
            
//   edited by Monir
//            self.buttonsView.frame=CGRectMake(0, [UIScreen mainScreen].bounds.size.height-80-55 , [UIScreen mainScreen].bounds.size.width , 80+55);
            
            //setting for iphone x
            
            if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone) {
                
                switch ((int)[[UIScreen mainScreen] nativeBounds].size.height) {
                        
                    case 1792:
                    case 2436:
                    case 2688:

                        printf("iPhone X.........");
                        
                        self.titleLabel.frame = CGRectMake(0, 44 , [UIScreen mainScreen].bounds.size.width , 44);
                        
                         self.buttonsView.frame=CGRectMake(0, [UIScreen mainScreen].bounds.size.height-50-54-20 , [UIScreen mainScreen].bounds.size.width , 50+54);
                        
                        self.scrollView.frame = CGRectMake(0, 88, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-88-self.buttonsView.frame.size.height-20);

                        break;
                    default:
                        printf("unknown");
                         self.buttonsView.frame=CGRectMake(0, [UIScreen mainScreen].bounds.size.height-50-54 , [UIScreen mainScreen].bounds.size.width , 50+54);
                        self.scrollView.frame = CGRectMake(0, 44, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-44-self.buttonsView.frame.size.height);
                }
            }
            
           
//
            
        }
        
        
// edited by Monir
        
//        self.nativeExpressAdView.frame=CGRectMake(0,55 , [UIScreen mainScreen].bounds.size.width , 80);
        
//
    
        
 // edited by Monir
    self.admobBannerView.adSize=kGADAdSizeSmartBannerPortrait;
        self.admobBannerView.adUnitID = /*ADMOB_BANNER_ID*/ @"ca-app-pub-8340924500267413/8079283084" ;
        self.admobBannerView.rootViewController = self;
        self.admobBannerView.delegate=self;
        
//
        
        GADRequest *request = [GADRequest request];
        request.testDevices = @[
                                kGADSimulatorID,
                                TEST_DEVICE_1,
                                TEST_DEVICE_2,
                                TEST_DEVICE_3,
                                ];
        
// edited by Monir
        [self.admobBannerView loadRequest:request];
        [self.admobBannerView loadRequest:[GADRequest request]];
//
        
 // edited by Monir

//    self.nativeExpressAdView.adSize=GADAdSizeFullWidthPortraitWithHeight(80);
//        self.nativeExpressAdView.adUnitID = ADMOB_NATIVE_ID;
//        self.nativeExpressAdView.rootViewController = self;
//        self.nativeExpressAdView.delegate=self;
//        [self.nativeExpressAdView loadRequest:request];
        
//        NSLog(@"self.nativeExpressAdView %@",self.nativeExpressAdView);
        
//
        
        self.interstitial = [self createAndLoadInterstitial];
        
    }
    else
    {
        NSLog(@"ad removed");
        [self.admobBannerView removeFromSuperview];
        [self.nativeExpressAdView removeFromSuperview];
        self.interstitial=nil;
        
        //setting for iphone x
        
        if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone) {
            
            switch ((int)[[UIScreen mainScreen] nativeBounds].size.height) {
                    
                case 1792:
                case 2436:
                case 2688:

                    printf("iPhone X");
                    self.buttonsView.frame=CGRectMake(0, [UIScreen mainScreen].bounds.size.height-75 , [UIScreen mainScreen].bounds.size.width , 55);
                     self.scrollView.frame = CGRectMake(0, 88, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-88-self.buttonsView.frame.size.height-20);
                    
                    break;
                default:
                    printf("unknown");
                    self.buttonsView.frame=CGRectMake(0, [UIScreen mainScreen].bounds.size.height-55 , [UIScreen mainScreen].bounds.size.width , 55);
                    self.scrollView.frame = CGRectMake(0, 44, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-44-self.buttonsView.frame.size.height);
            }
        }else{
            
            self.buttonsView.frame=CGRectMake(0, [UIScreen mainScreen].bounds.size.height-55 , [UIScreen mainScreen].bounds.size.width , 55);
            self.scrollView.frame = CGRectMake(0, 44, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-44-self.buttonsView.frame.size.height);
            
            
        }
        
        
    }
}

#pragma mark - BUTTONS ================================
- (IBAction)savePicButt:(id)sender {
    [self savePic];
}
- (IBAction)editPicButt:(id)sender {
    [self editPic];
}



#pragma mark - ACTIONS CALLED BY THE BUTTONS  ========================
// Sets a new Image/Picture up
- (void)newPic {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    HomeVC *homeVC = (HomeVC *)[storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
    homeVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:homeVC animated:true completion:nil];
}


// Open the Image Editor to edit your Picture
- (void)editPic {
    if(_imageView.image) {
        ImageEditor *editor = [[ImageEditor alloc] initWithImage:_imageView.image delegate:self];
        [self presentViewController:editor animated:true completion:nil];

    } else {
        [self newPic];
    }
}

// Save the edited image (with sharing options)
- (void)savePic {
    
    BOOL israteGiven = [[NSUserDefaults standardUserDefaults]  boolForKey:@"rated"];
    NSInteger isApproved  = [[NSUserDefaults standardUserDefaults]  integerForKey:@"isItunesApproved"];
    
    if (!israteGiven && isApproved==1) {
        
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Need To Save Or Share?" message:@"Leave us a Review & 5* Rating to Save And Share On social Networking" delegate:self cancelButtonTitle:@"No, Thanks" otherButtonTitles:@"Review", nil];
        alert.tag = 1;
        [alert show];
        
    }
    
    else{
    
    if(_imageView.image){
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SharingVC *shVC = (SharingVC *)[storyboard instantiateViewControllerWithIdentifier:@"SharingVC"];
        
        // Pass the Edited Image to the SharingScreen
        imageToBeShared = _imageView.image;
        
        [self presentViewController:shVC animated:true completion:nil];
        
       
        
    // Open the HomeVC where you can choose where to pick your picture up
    } else {
        [self newPic];
    }
    }
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    switch (buttonIndex) {
        case 0:
        {
            
        }
            break;
        case 1:
        {
            
            [self Rate];
            
            NSUserDefaults *userDefault   = [NSUserDefaults standardUserDefaults];
            [userDefault setBool:YES forKey:@"rated"];
            [userDefault synchronize];
            
        }
            break;
        default:
            break;
    }
}

-(void)Rate
{
    
#define RATE_LINK_IOS7 @"itms-apps://itunes.apple.com/app/id1001125129"
#define RATE_LINK_IOS6 @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=1001125129"
    
    if (([[[UIDevice currentDevice] systemVersion] compare:@"7.0" options:NSNumericSearch] != NSOrderedAscending)) {
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:[NSString stringWithFormat:RATE_LINK_IOS7]]];
    }
    else
    {
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:RATE_LINK_IOS6]];
    }
    
}





#pragma mark- IMAGE EDITOR DELEGATE ==========================

- (void)imageEditor:(ImageEditor *)editor didFinishEdittingWithImage:(UIImage *)image {
    NSLog(@"didFinishEdittingWithImage");
    _imageView.image = image;
    [self refreshImageView];
    
//    if([SKStoreReviewController class]){
//        [SKStoreReviewController requestReview] ;
//    }

        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:APP_NAME
                                     message:NSLocalizedString(@"Do you want save this image to Photo Gallery?", @"")
                                     preferredStyle:UIAlertControllerStyleAlert];
    
    
    
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:NSLocalizedString(@"Yes",@"")
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        NSLog(@"yes button clicked");
                                        
                                        //save to library
                                        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
                                        
                                        
                                        [editor dismissViewControllerAnimated:YES completion:^{
                                            if (self.interstitial.isReady) {
                                                [self.interstitial presentFromRootViewController:self];
                                            } else {
                                                NSLog(@"Ad wasn't ready");
                                            }
                                        }];

                                        
                                    }];
    
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"No",@"")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       NSLog(@"no button clicked");
                                       
                                       [editor dismissViewControllerAnimated:YES completion:^{
                                           if (self.interstitial.isReady) {
                                               [self.interstitial presentFromRootViewController:self];
                                           } else {
                                               NSLog(@"Ad wasn't ready");
                                           }
                                       }];

                                       
                                   }];
    
        [alert addAction:yesButton];
        [alert addAction:noButton];
    
        [editor presentViewController:alert animated:YES completion:nil];

    
    
    
}

- (void)imageEditorDidCancel:(ImageEditor*)editor;
{
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:APP_NAME
                                 message:NSLocalizedString(@"Are you sure, you want to quit without saving your image?", @"")
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:NSLocalizedString(@"Yes",@"")
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    NSLog(@"yes button clicked");
                                    
                                    
                                    [editor dismissViewControllerAnimated:YES completion:^{
                                        if (self.interstitial.isReady) {
                                            [self.interstitial presentFromRootViewController:self];
                                        } else {
                                            NSLog(@"Ad wasn't ready");
                                        }
                                    }];
                                    
                                }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"No",@"")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   NSLog(@"no button clicked");
                                   
                               }];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [editor presentViewController:alert animated:YES completion:nil];
   
    
}

- (void)imageEditor:(ImageEditor *)editor willDismissWithImageView:(UIImageView *)imageView canceled:(BOOL)canceled {
   
    [self refreshImageView];
}




#pragma mark- IMAGE SCROLLVIEW SETTINGS ======================

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return _imageView.superview;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    CGFloat Ws = _scrollView.frame.size.width - _scrollView.contentInset.left - _scrollView.contentInset.right;
    CGFloat Hs = _scrollView.frame.size.height - _scrollView.contentInset.top - _scrollView.contentInset.bottom;
    CGFloat W = _imageView.superview.frame.size.width;
    CGFloat H = _imageView.superview.frame.size.height;
    
    CGRect rct = _imageView.superview.frame;
    rct.origin.x = MAX((Ws-W)/2, 0);
    rct.origin.y = MAX((Hs-H)/2, 0);
    _imageView.superview.frame = rct;
}



// Refresh the ImageView ===========================
- (void)refreshImageView  {
    [self resetImageViewFrame];
    [self resetZoomScaleWithAnimate: false];
}

- (void)resetImageViewFrame {
    CGSize size = (_imageView.image) ? _imageView.image.size : _imageView.frame.size;
    CGFloat ratio = MIN(_scrollView.frame.size.width / size.width, _scrollView.frame.size.height / size.height);
    CGFloat W = ratio * size.width;
    CGFloat H = ratio * size.height;
    _imageView.frame = CGRectMake(0, 0, W, H);
    _imageView.superview.bounds = _imageView.bounds;
    
}

- (void)resetZoomScaleWithAnimate:(BOOL)animated {
    CGFloat Rw = _scrollView.frame.size.width / _imageView.frame.size.width;
    CGFloat Rh = _scrollView.frame.size.height / _imageView.frame.size.height;
    
    CGFloat scale = 1;
    Rw = MAX(Rw, _imageView.image.size.width / (scale * _scrollView.frame.size.width));
    Rh = MAX(Rh, _imageView.image.size.height / (scale * _scrollView.frame.size.height));
    
    _scrollView.contentSize = _imageView.frame.size;
    _scrollView.minimumZoomScale = 1;
    _scrollView.maximumZoomScale = MAX(MAX(Rw, Rh), 1);
    
    [_scrollView setZoomScale:_scrollView.minimumZoomScale animated:animated];
    [self scrollViewDidZoom:_scrollView];
}

#pragma mark - GADNativeExpressAdViewDelegate

- (void)nativeExpressAdViewDidReceiveAd:(GADNativeExpressAdView *)nativeExpressAdView {
    if (nativeExpressAdView.videoController.hasVideoContent) {
        NSLog(@"Received ad an with a video asset.");
    } else {
        NSLog(@"Received ad an without a video asset.");
    }
}

-(void)nativeExpressAdView:(GADNativeExpressAdView *)nativeExpressAdView didFailToReceiveAdWithError:(GADRequestError *)error
{
    NSLog(@"didFailToReceiveAdWithError with Error %@",error);
    nativeExpressAdView.hidden=YES;
}
#pragma mark - GADBannerViewDelegate

- (void)adViewDidReceiveAd:(GADBannerView *)bannerView {
    bannerView.hidden = NO;
    NSLog(@"adView:adViewDidReceiveAd");

}

- (void)adView:(GADBannerView *)adView didFailToReceiveAdWithError:(GADRequestError *)error {
    adView.hidden = YES;
    NSLog(@"adView:didFailToReceiveAdWithError: %@", error.localizedDescription);
}


- (GADInterstitial *)createAndLoadInterstitial {
    
    NSLog(@"createAndLoadInterstitial");
    
    GADInterstitial *interstitial =
    [[GADInterstitial alloc] initWithAdUnitID:ADMOB_INTERSTITIAl_ID];
    interstitial.delegate = self;
    
    GADRequest *request = [GADRequest request];
    request.testDevices = @[ kGADSimulatorID, TEST_DEVICE_1,TEST_DEVICE_2,TEST_DEVICE_3 ];
    [interstitial loadRequest:request];
    //[interstitial loadRequest:[GADRequest request]];
    
    return interstitial;
    
    
}


- (void)interstitialDidDismissScreen:(GADInterstitial *)interstitial {
    self.interstitial = [self createAndLoadInterstitial];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end

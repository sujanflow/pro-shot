/*============================
 
 Pro Shot
 
 iOS 7/8 iPhone Photo Editor App template
 created by FV iMAGINATION - 2014
 http://www.fvimagination.com
 
 ==============================*/


#import "HomeVC.h"
#import "PreviewVC.h"
#import "IAPController.h"
#import "Configs.h"


@interface HomeVC()
@end



@implementation HomeVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

// Hide Status Bar
- (BOOL)prefersStatusBarHidden {
    return true;
}
// Prevent the StatusBar from showing up after picking an image
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:true];
}



#pragma mark - VIEW DID LOAD ========================
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Get a random background image
    NSInteger randomBkg = arc4random()%5;
    _bkgImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"bkg%ld", (long)randomBkg]];
    
    // Localize text
    _takeApicLabel.text = NSLocalizedString(@"Take a Picture", @"");
    _pickFromLibLabel.text = NSLocalizedString(@"Choose from Library", @"");
    _sampleImageLabel.text = NSLocalizedString(@"Use background image", @"");
    
    
    // Round the buttons corners
    _libraryOutlet.layer.cornerRadius = 20;
    _cameraOutlet.layer.cornerRadius = 20;
    _sampleImageOutlet.layer.cornerRadius = 20;
    _logoImage.layer.cornerRadius = 30;
    
    
    
    //for testing
    
   // [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isAdRemoved"];
    
}


- (IBAction)cameraButt:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CameraViewController *camVC = (CameraViewController *)[storyboard instantiateViewControllerWithIdentifier:@"CameraViewController"];
    [self presentViewController:camVC animated: true completion:nil];
}



#pragma mark - PHOTO LIBRARY BUTTON ===================
- (IBAction)photoLibraryButt:(id)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    [picker setDelegate:self];
    [picker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [self presentViewController:picker animated:true completion:nil];
}




#pragma mark - IMAGE PICKER DELEGATE ===================
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
        
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    [self dismissViewControllerAnimated:false completion:nil];

    // Go to the Main Screen
    dispatch_async(dispatch_get_main_queue(), ^{
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        PreviewVC *prevVC = (PreviewVC *)[storyboard instantiateViewControllerWithIdentifier:@"PreviewVC"];
//        
//        // Passing Image to Preview VC
//        passedImage = image;
//        
//        prevVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//        [self presentViewController:prevVC animated:true completion:nil];
        TOCropViewController *cropController = [[TOCropViewController alloc] initWithCroppingStyle:TOCropViewCroppingStyleDefault image:image];
        cropController.delegate = self;
        cropController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self presentViewController:cropController animated:YES completion:nil];
        
    });
}



#pragma mark - SAMPLE IMAGE BUTTON =======================
- (IBAction)sampleImageButt:(id)sender {
    
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    PreviewVC *prevVC = (PreviewVC *)[storyboard instantiateViewControllerWithIdentifier:@"PreviewVC"];
//    
//    // Passing Image to Main Screen
//    passedImage = _bkgImage.image;
//    
//    prevVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//    [self presentViewController:prevVC animated:true completion:nil];
    
    
//    TOCropViewController *cropController = [[TOCropViewController alloc] initWithCroppingStyle:TOCropViewCroppingStyleDefault image:_bkgImage.image];
//    cropController.delegate = self;
//    cropController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//    [self presentViewController:cropController animated:YES completion:nil];
    
    
    [[UIApplication sharedApplication] openURL:[NSURL
                                                URLWithString: @"https://itunes.apple.com/US/app/id1249824876"]];
    
}

#pragma mark - Cropper Delegate -
-(void)cropViewController:(TOCropViewController *)cropViewController didFinishCancelled:(BOOL)cancelled
{
    // [self updateImageViewWithImage:_capturedImageV.image fromCropViewController:cropViewController];
    [cropViewController dismissViewControllerAnimated:YES completion:^{
        NSLog(@"Crop cancel");
        
    }];
    
}

- (void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle
{
    
    //
    //    self.croppedFrame = cropRect;
    //    self.angle = angle;
    //    [self updateImageViewWithImage:image fromCropViewController:cropViewController];
    //
    //
    
     [cropViewController dismissViewControllerAnimated:YES completion:^{
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            PreviewVC *prevVC = (PreviewVC *)[storyboard instantiateViewControllerWithIdentifier:@"PreviewVC"];
            
            // Passing Image to Main Screen
            passedImage = image;
            
            prevVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            [self presentViewController:prevVC animated:true completion:nil];
        });
        
    }];

    
}

- (void)updateImageViewWithImage:(UIImage *)image fromCropViewController:(TOCropViewController *)cropViewController
{
    NSLog(@"updateImageViewWithImage");
    
    [self layoutImageView];
    
    
}

#pragma mark - Image Layout -
- (void)layoutImageView
{
    
    NSLog(@"layoutImageView");
    //    if (self.imageView.image == nil)
    //        return;
    //
    //    CGFloat padding = 20.0f;
    //
    //    CGRect viewFrame = self.view.bounds;
    //    viewFrame.size.width -= (padding * 2.0f);
    //    viewFrame.size.height -= ((padding * 2.0f));
    //
    //    CGRect imageFrame = CGRectZero;
    //    imageFrame.size = self.imageView.image.size;
    //
    //    if (self.imageView.image.size.width > viewFrame.size.width ||
    //        self.imageView.image.size.height > viewFrame.size.height)
    //    {
    //        CGFloat scale = MIN(viewFrame.size.width / imageFrame.size.width, viewFrame.size.height / imageFrame.size.height);
    //        imageFrame.size.width *= scale;
    //        imageFrame.size.height *= scale;
    //        imageFrame.origin.x = (CGRectGetWidth(self.view.bounds) - imageFrame.size.width) * 0.5f;
    //        imageFrame.origin.y = (CGRectGetHeight(self.view.bounds) - imageFrame.size.height) * 0.5f;
    //        self.imageView.frame = imageFrame;
    //    }
    //    else {
    //        self.imageView.frame = imageFrame;
    //        self.imageView.center = (CGPoint){CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds)};
    //    }
}



#pragma mark - SETTINGS BUTTON =======================
- (IBAction)settingsButt:(id)sender {
    
}



#pragma mark - INSTAGRAM PAGE BUTTON =======================
- (IBAction)instagramButt:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL
    URLWithString: INSTAGRAM_URL]];
}



#pragma mark - STORE BUTTON =======================
- (IBAction)storeButt:(id)sender {
    IAPController *iapVC = [[IAPController alloc]initWithNibName:@"IAPController" bundle:nil];
    [self presentViewController: iapVC animated:true completion:nil];
}






- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

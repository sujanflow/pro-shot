/*========================================
 
 - Instafun -
 
 made by FV iMAGINATION ©2015
 All rights reserved
 
 ========================================*/


#import "IAPController.h"
#import "Configs.h"
#import "ReadNWrite.h"

@interface IAPController ()
@end

@implementation IAPController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    
#ifdef DEBUG
    for (SKPaymentTransaction *transaction in [[SKPaymentQueue defaultQueue] transactions]) {
        NSLog(@"finish transactions pending sind last load...");
        [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    }
#endif
    
    NSMutableArray *pListArray = [[NSMutableArray alloc] init];
    pListArray = [ReadNWrite readFromDoucmentDirectory:@"userInfo.plist"];
    
    if(pListArray.count && (![[[pListArray objectAtIndex:0] objectForKey:@"iapMade"] isEqual:[NSNull null]]))
    {
        //NSLog(@"pListArray pListArray");
        iapMade= [[[pListArray objectAtIndex:0] objectForKey:@"iapMade"] boolValue];
        
    }else
    {
        
        iapMade= [[NSUserDefaults standardUserDefaults] boolForKey:@"iapMade"];

    }
    
   // iapMade= [[NSUserDefaults standardUserDefaults] boolForKey:@"iapMade"];
    isAdRemoved=[[NSUserDefaults standardUserDefaults] boolForKey:@"isAdRemoved"];
    
    NSLog(@"isAdRemoved %d",isAdRemoved);
    
    
    // ADD PROGRESS HUD ================
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    [hud show:true];
    

    // IAPhelper init =============================
    _IAPHelper = [[IAPhelper alloc] init];
	_IAPHelper.delegate = self;

    // IMPORTANT: here you must place the same Product ID
    // you've created into your IAP settings in iTunes 
	[_IAPHelper fetchProducts:@[IAP_PRODUCT]];
    //============================================================
    
    
    // Set Texts
    _titleLabel.text = NSLocalizedString(@"Pro Shot Premium", @"");
    [_buyOutlet setTitle:NSLocalizedString(@"Get Premium Now", @"") forState:UIControlStateNormal];
    [_removeAdOutletButton setTitle:NSLocalizedString(@"Remove Ads", @"") forState:UIControlStateNormal];
    [_removeAdOutletButton setTitle:NSLocalizedString(@"Reset Ads", @"") forState:UIControlStateSelected];
    
    [_restorePurchaseOutlet setTitle:NSLocalizedString(@"Restore Purchase", @"") forState:UIControlStateNormal];
    
    
    //setting for iphone x
    
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone) {
        
        switch ((int)[[UIScreen mainScreen] nativeBounds].size.height) {
                
            case 1792:
            case 2436:
            case 2688:
                printf("iPhone X");
                
                self.titleLabel.center = CGPointMake(self.titleLabel.center.x, self.titleLabel.center.y+44);
                self.dismissButton.center =CGPointMake(self.dismissButton.center.x, self.dismissButton.center.y+44);
                
                break;
            default:
                printf("unknown");
                
        }
    }
    


}

-(void)viewWillAppear:(BOOL)animated {
    
    if (!iapMade) {
        _buyOutlet.hidden = false;
       // _removeAdOutletButton.hidden = true;
    } else {
        _buyOutlet.hidden = true;
       // _removeAdOutletButton.hidden = false;
    }
    
    
//    if (!isAdRemoved) {
//        _removeAdOutletButton.selected = false;
//    } else {
//        _removeAdOutletButton.selected = true;
//    }
    
    
    
}



- (IBAction)dimissButt:(id)sender {
    
    [self dismissViewControllerAnimated:true completion:nil];
}


#pragma mark - RESTORE PURCHASE BUTTON ==============================
- (IBAction)restorePurchaseButt:(id)sender {
    
    [_IAPHelper restorePurchases];
    
    [hud show:true];
}


#pragma mark - BUY PREMIUM BUTTON ==============================
- (IBAction)buyButt:(id)sender {
	// Initialize IAP Product by calling SKProduct
    
    [_IAPHelper purchaseItem:_product];

    [hud show:true];
    
   

}

#pragma mark - Remove Ad BUTTON ==============================

- (IBAction)removeAdButton:(UIButton *)sender {
    
    sender.selected=!sender.selected;
    isAdRemoved = !isAdRemoved;
    [[NSUserDefaults standardUserDefaults] setBool:isAdRemoved forKey:@"isAdRemoved"];
    
    
//    iapMade = !iapMade;
//    [[NSUserDefaults standardUserDefaults] setBool:iapMade forKey:@"iapMade"];
//    
    
}




#pragma mark - IAP DELEGATE METHODS ==============================

// iTunes Store connection failed...
- (void)bankerFailedToConnect {
    UIAlertView *av = [[UIAlertView alloc] initWithTitle: APP_NAME
    message:NSLocalizedString(@"iTunes Store connection failed, try again!", @"") delegate:nil
    cancelButtonTitle:NSLocalizedString(@"OK", @"")
    otherButtonTitles:nil];
	[av show];
	
    [hud hide:false];

}

// NO IAP Products for this app...
- (void)bankerNoProductsFound {
    UIAlertView *av = [[UIAlertView alloc] initWithTitle: APP_NAME
    message:NSLocalizedString(@"iTunes Store connection failed, try again!", @"") delegate:nil
    cancelButtonTitle:NSLocalizedString(@"OK", @"")
    otherButtonTitles:nil];
	[av show];
	
    [hud hide:false];
}

// IAP Product(s) found
- (void)bankerFoundProducts:(NSArray *)products {
    _product = [products objectAtIndex:0];
    
    _descTxt.text = [NSString stringWithFormat:@"%@ \n %@", _product.localizedDescription, _product.localizedPrice];
    _descTxt.font = [UIFont fontWithName:@"BebasNeue" size:22];
    _descTxt.textColor = [UIColor whiteColor];
    _descTxt.textAlignment = NSTextAlignmentCenter;
    
    [hud hide:false];
}
// Invalid IAP Products found...
- (void)bankerFoundInvalidProducts:(NSArray *)products {
    UIAlertView *av = [[UIAlertView alloc] initWithTitle: APP_NAME
    message:NSLocalizedString(@"There are no valid products available, try again!", @"") delegate:nil
    cancelButtonTitle:NSLocalizedString(@"OK", @"")
    otherButtonTitles:nil];
	[av show];
	
    [hud hide:false];
}


// IAP purchase successfully made...
- (void)bankerProvideContent:(SKPaymentTransaction *)paymentTransaction {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setBool:true forKey:@"InAppPurchase"];
	[defaults synchronize];
	
	UIAlertView *av = [[UIAlertView alloc]
    initWithTitle:NSLocalizedString(@"Purchase successful!", @"")
    message:NSLocalizedString(@"Enjoy Pro Shot Premium!", @"")
    delegate:self
    cancelButtonTitle:NSLocalizedString(@"OK", @"")
    otherButtonTitles:nil];
	[av show];
    
    
    // Hide views
    [hud hide:false];
    _buyOutlet.hidden = true;
}


// IAP Payment complete...
- (void)bankerPurchaseComplete:(SKPaymentTransaction *)paymentTransaction {
    //transaction.payment.productIdentifier
    // BOOL iapMade = TRUE, save and store it forever
   // [hud hide:false];
    
    iapMade = true;
    [[NSUserDefaults standardUserDefaults] setBool:iapMade forKey:@"iapMade"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    isAdRemoved = true;
    [[NSUserDefaults standardUserDefaults] setBool:isAdRemoved forKey:@"isAdRemoved"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSMutableArray *pListArray = [[NSMutableArray alloc] init];
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    
    [dictionary setObject:[NSNumber numberWithBool:iapMade] forKey:@"iapMade"];
    [pListArray addObject:dictionary];
    [ReadNWrite writeToDucumentDirectory:@"userInfo.plist" :pListArray];
    
    NSLog(@"bankerPurchaseComplete");
    
    // Recall method that will reset the Overlays List
    [self viewWillAppear:true];
    
}

// IAP Payment failed...
- (void)bankerPurchaseFailed:(NSString *)productIdentifier withError:(NSString *)errorDescription {
    UIAlertView *av = [[UIAlertView alloc] initWithTitle: APP_NAME
    message:NSLocalizedString(@"Purchase has failed, sorry. Try again later!", @"") delegate:nil
    cancelButtonTitle:NSLocalizedString(@"OK", @"")
    otherButtonTitles:nil];
	[av show];
	
    [hud hide:false];
}

// IAP Purchase cancelled by the user...
- (void)bankerPurchaseCancelledByUser:(NSString *)productIdentifier {

    _buyOutlet.hidden = false;
    [hud hide:false];
}

// Restore Purchases failed...
- (void)bankerFailedRestorePurchases {
    
    NSLog(@"bankerFailedRestorePurchases");
    [hud hide:false];
	_buyOutlet.hidden = false;

}

// Purchase restored
- (void)bankerDidRestorePurchases {
    
    [hud hide:false];
    
    // BOOL unlockOverlays = TRUE, save and store it forever
    iapMade = true;
    [[NSUserDefaults standardUserDefaults] setBool:iapMade forKey:@"iapMade"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    _buyOutlet.hidden = true;
    
    
    isAdRemoved = true;
    [[NSUserDefaults standardUserDefaults] setBool:isAdRemoved forKey:@"isAdRemoved"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSMutableArray *pListArray = [[NSMutableArray alloc] init];
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    
    [dictionary setObject:[NSNumber numberWithBool:iapMade] forKey:@"iapMade"];
    [pListArray addObject:dictionary];
    [ReadNWrite writeToDucumentDirectory:@"userInfo.plist" :pListArray];
    
    
    // Recall method that will reset the Overlays List
    [self viewWillAppear:true];
    
}

/*================== END IAP METHODS ==============================*/




- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}

@end

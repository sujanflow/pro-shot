/*========================================
 
 - Instafun -
 
 made by FV iMAGINATION ©2015
 All rights reserved
 
 ========================================*/


#import <UIKit/UIKit.h>
#import "IAPhelper.h"
#import "MBProgressHUD.h"

BOOL iapMade;
BOOL isAdRemoved;
BOOL iapForSticker;
BOOL iapForFrame;
BOOL iapForFonts;
BOOL iapForEffects;
MBProgressHUD *hud;

@protocol IAPControllerDelegate <NSObject>
@required
- (void)inAppScreenClosed;
@end

@interface IAPController : UIViewController
<
IAPhelperDelegate
>


// InApp Purchase Properties  =========
@property (strong, nonatomic) IAPhelper *IAPHelper;
@property (strong, nonatomic) SKProduct *product;

// Buttons
@property (weak, nonatomic) IBOutlet UIButton *buyOutlet;
@property (weak, nonatomic) IBOutlet UIButton *removeAdOutletButton;
@property (weak, nonatomic) IBOutlet UIButton *restorePurchaseOutlet;
@property (weak, nonatomic) IBOutlet UIButton *dismissButton;



// Labels
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextView *descTxt;

@property (weak, nonatomic) UIViewController <IAPControllerDelegate> *delegate;

@end

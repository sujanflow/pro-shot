//
//  IAPhelper.h
//
//  Created by FV iMAGINATION
//  Copyright (c) 2014 FV iMAGINATION
//

#import <Foundation/Foundation.h>

@import StoreKit;

@protocol IAPhelperDelegate <NSObject>
@required
- (void)bankerFailedToConnect;
- (void)bankerNoProductsFound;
- (void)bankerFoundProducts:(NSArray *)products;
- (void)bankerFoundInvalidProducts:(NSArray *)products;
- (void)bankerProvideContent:(SKPaymentTransaction *)paymentTransaction;
- (void)bankerPurchaseComplete:(SKPaymentTransaction *)paymentTransaction;
- (void)bankerPurchaseFailed:(NSString *)productIdentifier withError:(NSString *)errorDescription;
- (void)bankerPurchaseCancelledByUser:(NSString *)productIdentifier;
- (void)bankerFailedRestorePurchases;

@optional
- (void)bankerDidRestorePurchases;
- (void)bankerCanNotMakePurchases;
- (void)bankerContentDownloadComplete:(SKDownload *)download;
- (void)bankerContentDownloading:(SKDownload *)download;

@end


@interface IAPhelper: NSObject
<
SKPaymentTransactionObserver,
SKProductsRequestDelegate
>

+ (IAPhelper *)sharedInstance;

@property (weak, nonatomic) UIViewController <IAPhelperDelegate> *delegate;
@property (strong, nonatomic) SKProductsRequest *productsRequest;

- (void)fetchProducts:(NSArray *)productIdentifiers;
- (void)purchaseItem:(SKProduct *)product;
- (void)restorePurchases;

- (BOOL)canMakePurchases;

@end


@interface SKProduct (LocalizedPrice)

@property (nonatomic, readonly) NSString *localizedPrice;

@end

@implementation SKProduct (LocalizedPrice)

- (NSString *)localizedPrice {
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numberFormatter setLocale:self.priceLocale];
    NSString *formattedString = [numberFormatter stringFromNumber:self.price];
    return formattedString;
}

@end

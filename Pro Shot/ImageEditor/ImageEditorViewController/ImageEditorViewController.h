/*============================
 
 Pro Shot
 
 iOS 7/8 iPhone Photo Editor App template
 created by FV iMAGINATION - 2014
 http://www.fvimagination.com
 
 ==============================*/

#import "ImageEditor.h"
#import <GoogleMobileAds/GoogleMobileAds.h>
#import <FBAudienceNetwork/FBAudienceNetwork.h>
#import "IAPController.h"

BOOL isAdRemoved;

@interface ImageEditorViewController : ImageEditor
<
UIScrollViewDelegate,
UIBarPositioningDelegate,
GADBannerViewDelegate,
GADNativeExpressAdViewDelegate,
GADInterstitialDelegate,
FBAdViewDelegate,
FBInterstitialAdDelegate,
IAPControllerDelegate
>
{
    IBOutlet __weak UINavigationBar *_navigationBar;
}

@property (weak, nonatomic) IBOutlet UIBarButtonItem *rightButtItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *leftButtItem;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (nonatomic, strong) UIImageView  *imageView;
@property (nonatomic, weak) IBOutlet UIScrollView *menuView;
@property (strong, nonatomic) GADBannerView *bannerView;
@property (strong, nonatomic) GADNativeExpressAdView *nativeAdView;
@property (nonatomic, strong) GADInterstitial *interstitial;

@property (strong, nonatomic) FBAdView *fbBannerView;
@property (nonatomic, strong) FBInterstitialAd *fbInterstitialAd;

- (IBAction)pushedCloseBtn:(id)sender;
- (IBAction)pushedFinishBtn:(id)sender;


- (id)initWithImage:(UIImage*)image;

- (void)fixZoomScaleWithAnimated:(BOOL)animated;
- (void)resetZoomScaleWithAnimated:(BOOL)animated;



@end

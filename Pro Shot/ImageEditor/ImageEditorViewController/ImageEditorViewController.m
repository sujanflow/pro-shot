/*============================
 
 Pro Shot
 
 iOS 7/8 iPhone Photo Editor App template
 created by FV iMAGINATION - 2014
 http://www.fvimagination.com
 
 ==============================*/


#import "ImageEditorViewController.h"
#import "ImageToolBase.h"
#import "Configs.h"


@interface ImageEditorViewController()
<
ImageToolProtocol
>
@property (nonatomic, strong) ImageToolBase *currentTool;
@property (nonatomic, strong, readwrite) ImageToolInfo *toolInfo;
@property (nonatomic, strong) UIImageView *targetImageView;
@end




@implementation ImageEditorViewController
{
    UIImage *_originalImage;
    UIView *_bgView;
}
@synthesize toolInfo = _toolInfo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (id)init
{
    self = [self initWithNibName:@"ImageEditorViewController" bundle:nil];
    if (self){
        _toolInfo = [ImageToolInfo toolInfoForToolClass:[self class]];
    }
    return self;
}

- (id)initWithImage:(UIImage *)image
{
    return [self initWithImage:image delegate:nil];
}

- (id)initWithImage:(UIImage*)image delegate:(id<ImageEditorDelegate>)delegate
{
    self = [self init];
    if (self){
        _originalImage = [image deepCopy];
        self.delegate = delegate;
    }
    return self;
}

- (id)initWithDelegate:(id<ImageEditorDelegate>)delegate
{
    self = [self init];
    if (self){
        self.delegate = delegate;
    }
    return self;
}



- (void)viewWillAppear:(BOOL)animated {
    
    NSLog(@"viewWillAppear in imageeditor");
    
    // Set the Title's font and color
    _navigationBar.titleTextAttributes =   [NSDictionary dictionaryWithObjectsAndKeys:
    [UIColor darkGrayColor],
    NSForegroundColorAttributeName, NAVBAR_FONT,
    NSFontAttributeName,nil];
    
    isAdRemoved=[[NSUserDefaults standardUserDefaults] boolForKey:@"isAdRemoved"];
    
    self.bannerView = [[GADBannerView alloc]
                                 initWithAdSize:kGADAdSizeSmartBannerPortrait];
    self.menuView.transform=CGAffineTransformIdentity;
    if(!isAdRemoved)
    {

//edited by Monir
//        self.nativeAdView=[[GADNativeExpressAdView alloc] init];
//
        
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
        {
            self.menuView.frame=CGRectMake(0, [UIScreen mainScreen].bounds.size.height-90-55 , [UIScreen mainScreen].bounds.size.width , 55);
            self.bannerView.frame=CGRectMake(0, self.menuView.top+55 , [UIScreen mainScreen].bounds.size.width , 90);
            self.fbBannerView = [[FBAdView alloc] initWithPlacementID:FB_BANNER_ID
                                                               adSize:kFBAdSizeHeight90Banner
                                                   rootViewController:self];
//  edited by Monir
//            self.nativeAdView.frame=CGRectMake(0, self.menuView.top+55 , [UIScreen mainScreen].bounds.size.width , 90);
//            self.nativeAdView.adSize=GADAdSizeFullWidthPortraitWithHeight(90);
//
            
            
        }
        else
        {
            
            //setting for iphone x
            
            if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone) {
                
                switch ((int)[[UIScreen mainScreen] nativeBounds].size.height) {
                        
                    case 1792:
                    case 2436:
                    case 2688:

                        printf("iPhone X.........");
                        
                        _navigationBar.frame = CGRectMake(0, 40,_navigationBar.frame.size.width, _navigationBar.frame.size.height);
                        _navigationBar.layer.zPosition = 1001;
                        
                        
                        self.menuView.frame=CGRectMake(0, [UIScreen mainScreen].bounds.size.height-50-55-20 , [UIScreen mainScreen].bounds.size.width , 55);
                        self.scrollView.frame = CGRectMake(0,(_navigationBar.frame.size.height+50), [UIScreen mainScreen].bounds.size.width , [UIScreen mainScreen].bounds.size.height - (70 + _navigationBar.frame.size.height + self.menuView.frame.size.height + self.bannerView.frame.size.height));
                        self.bannerView.frame=CGRectMake(0, self.menuView.top+55 , [UIScreen mainScreen].bounds.size.width , 50);
                        self.fbBannerView = [[FBAdView alloc] initWithPlacementID:FB_BANNER_ID
                                                                           adSize:kFBAdSizeHeight50Banner
                                                               rootViewController:self];
                        
                        
                        break;
                    default:
                        printf("unknown");
                        
                        self.menuView.frame=CGRectMake(0, [UIScreen mainScreen].bounds.size.height-50-55 , [UIScreen mainScreen].bounds.size.width , 55);
                        self.bannerView.frame=CGRectMake(0, self.menuView.top+55 , [UIScreen mainScreen].bounds.size.width , 50);
                        self.fbBannerView = [[FBAdView alloc] initWithPlacementID:FB_BANNER_ID
                                                                           adSize:kFBAdSizeHeight50Banner
                                                               rootViewController:self];
                        
                }
            }

            

//edited by Monir
//            self.nativeAdView.frame=CGRectMake(0, self.menuView.top+55 , [UIScreen mainScreen].bounds.size.width , 80);
//
//            self.nativeAdView.adSize=GADAdSizeFullWidthPortraitWithHeight(80);
//
            
        }
        

        
//edited by Monir
        if(![self.view.subviews containsObject:self.bannerView])
        {
            [self.view addSubview:self.bannerView];
            NSLog(@"menuView %@",self.menuView);
            NSLog(@"bannerview %@",self.bannerView);
            
        }
        
        
//        if(![self.view.subviews containsObject:self.nativeAdView])
//        {
//            [self.view addSubview:self.nativeAdView];
//            NSLog(@"menuView %@",self.menuView);
//            NSLog(@"nativeAdView %@",self.nativeAdView);
//
//        }

        
        self.bannerView.adSize=kGADAdSizeSmartBannerPortrait;
        self.bannerView.adUnitID = /*ADMOB_BANNER_ID*/@"ca-app-pub-8340924500267413/9621206287";
        self.bannerView.rootViewController = self;
        self.bannerView.delegate=self;
//
        
        GADRequest *request = [GADRequest request];
      //  request.testDevices = @[kGADSimulatorID,TEST_DEVICE_1,TEST_DEVICE_2,TEST_DEVICE_3];
   
//edited by Monir
        [self.bannerView loadRequest:request];
        [self.bannerView loadRequest:[GADRequest request]];
        
//        self.nativeAdView.adUnitID = ADMOB_NATIVE_ID;
//        self.nativeAdView.rootViewController = self;
//        self.nativeAdView.delegate=self;
//        [self.nativeAdView loadRequest:request];
//
        
        self.interstitial = [self createAndLoadInterstitial];
        
        
        
        //Fb ad settings
        //[FBAdSettings setLogLevel:FBAdLogLevelLog];
        //[FBAdSettings addTestDevice:TEST_DEVICE_FOR_FB];
        
        self.fbBannerView.delegate = self;
        self.fbBannerView.frame=self.nativeAdView.frame;
        
        if(![self.view.subviews containsObject:self.fbBannerView])
        {
            [self.view addSubview:self.fbBannerView];
        }
        [self.fbBannerView loadAd];
        [self.fbBannerView setHidden:YES];

        [self loadFBInterstitial];
        
    }
    else
    {
        [self.bannerView removeFromSuperview];
        [self.nativeAdView removeFromSuperview];
        
        self.interstitial=nil;
        
        self.fbInterstitialAd=nil;
        
        //setting for iphone x
        
        if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone) {
            
            switch ((int)[[UIScreen mainScreen] nativeBounds].size.height) {
                    
                case 1792:
                case 2436:
                case 2688:

                    printf("iPhone X");
                    self.menuView.frame=CGRectMake(0, [UIScreen mainScreen].bounds.size.height-75 , [UIScreen mainScreen].bounds.size.width , 55);
                    break;
                default:
                    printf("unknown");
                    self.menuView.frame=CGRectMake(0, [UIScreen mainScreen].bounds.size.height-55 , [UIScreen mainScreen].bounds.size.width , 55);
            }
        }else{
            
            self.menuView.frame=CGRectMake(0, [UIScreen mainScreen].bounds.size.height-55 , [UIScreen mainScreen].bounds.size.width , 55);
        }

        
        
    }
    
    NSLog(@"self.menuView %@",self.menuView);
   

}

-(void)inAppScreenClosed
{
    [self.currentTool refreshViewAfterInApp];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.title = _toolInfo.title;
    
    [self resetImageViewFrame];
    
    self.view.frame = [UIScreen mainScreen].bounds;
    
    if([self respondsToSelector:@selector(automaticallyAdjustsScrollViewInsets)]){
        self.automaticallyAdjustsScrollViewInsets = false;
    }
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]){
        self.navigationController.interactivePopGestureRecognizer.enabled = false;
    }
       
    if (self.navigationController != nil){
        [self.navigationController setNavigationBarHidden:false animated:true];
        _navigationBar.hidden = true;
        [_navigationBar popNavigationItemAnimated:false];
    } else {
        _navigationBar.topItem.title = self.title;
    }
    
    [self setMenuView];
    
    if(_imageView==nil){
        _imageView = [UIImageView new];
        [_scrollView addSubview:_imageView];
    }
    
    //Viewwillappear
    
    [self resetZoomScaleWithAnimated:true];
    [self resetImageViewFrame];
    
    
    if (_targetImageView){
        [self expropriateImageView];
        
    } else {
        [self refreshImageView];
    }
    
    
    
}




#pragma mark- View transition

- (void)copyImageViewInfo:(UIImageView*)fromView toView:(UIImageView*)toView
{
    CGAffineTransform transform = fromView.transform;
    fromView.transform = CGAffineTransformIdentity;
    
    toView.transform = CGAffineTransformIdentity;
    toView.frame = [toView.superview convertRect:fromView.frame fromView:fromView.superview];
    toView.transform = transform;
    toView.image = fromView.image;
    toView.contentMode = fromView.contentMode;
    toView.clipsToBounds = fromView.clipsToBounds;
    
    fromView.transform = transform;
}


- (void)expropriateImageView
{
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    
    UIImageView *animateView = [UIImageView new];
    [window addSubview:animateView];
    [self copyImageViewInfo:self.targetImageView toView:animateView];
    
    _bgView = [[UIView alloc] initWithFrame:self.view.bounds];
    [self.view insertSubview:_bgView atIndex:0];
    
    _bgView.backgroundColor = self.view.backgroundColor;
    self.view.backgroundColor = [self.view.backgroundColor colorWithAlphaComponent:0];
    
    self.targetImageView.hidden = YES;
    _imageView.hidden = YES;
    _bgView.alpha = 0;
    _navigationBar.transform = CGAffineTransformMakeTranslation(0, -_navigationBar.height);
    _menuView.transform = CGAffineTransformMakeTranslation(0, self.view.height-_menuView.top);
    
    [UIView animateWithDuration:kImageToolAnimationDuration
                     animations:^{
                         animateView.transform = CGAffineTransformIdentity;
                         
                         CGFloat dy = ([UIDevice iosVersion]<7) ? [UIApplication sharedApplication].statusBarFrame.size.height : 0;
                         
                         CGSize size = (_imageView.image) ? _imageView.image.size : _imageView.frame.size;
                         if(size.width>0 && size.height>0){
                             CGFloat ratio = MIN(_scrollView.width / size.width, _scrollView.height / size.height);
                             CGFloat W = ratio * size.width;
                             CGFloat H = ratio * size.height;
                             animateView.frame = CGRectMake((_scrollView.width-W)/2 + _scrollView.left, (_scrollView.height-H)/2 + _scrollView.top + dy, W, H);
                         }
                         
                         _bgView.alpha = 1;
                         _navigationBar.transform = CGAffineTransformIdentity;
                         _menuView.transform = CGAffineTransformIdentity;
                     }
                     completion:^(BOOL finished) {
                         self.targetImageView.hidden = NO;
                         _imageView.hidden = NO;
                         [animateView removeFromSuperview];

                     }];
}

- (void)restoreImageView:(BOOL)canceled
{
    if(!canceled){
        self.targetImageView.image = _imageView.image;
    }
    self.targetImageView.hidden = true;
    
    id<ImageEditorTransitionDelegate> delegate = [self transitionDelegate];
    if([delegate respondsToSelector:@selector(imageEditor:willDismissWithImageView:canceled:)]){
        [delegate imageEditor:self willDismissWithImageView:self.targetImageView canceled:canceled];
    }
    
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    
    UIImageView *animateView = [UIImageView new];
    [window addSubview:animateView];
    [self copyImageViewInfo:_imageView toView:animateView];
    
    _menuView.frame = [window convertRect:_menuView.frame fromView:_menuView.superview];
    _navigationBar.frame = [window convertRect:_navigationBar.frame fromView:_navigationBar.superview];
    
    [window addSubview:_menuView];
    [window addSubview:_navigationBar];
    
    self.view.userInteractionEnabled = NO;
    _menuView.userInteractionEnabled = NO;
    _navigationBar.userInteractionEnabled = NO;
    _imageView.hidden = YES;
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         _bgView.alpha = 0;
                         _menuView.alpha = 0;
                         _navigationBar.alpha = 0;
                         
                         _menuView.transform = CGAffineTransformMakeTranslation(0, self.view.height-_menuView.top);
                         _navigationBar.transform = CGAffineTransformMakeTranslation(0, -_navigationBar.height);
                         
                         [self copyImageViewInfo:self.targetImageView toView:animateView];
                     }
                     completion:^(BOOL finished) {
                         [animateView removeFromSuperview];
                         [_menuView removeFromSuperview];
                         [_navigationBar removeFromSuperview];
                         
                         [self willMoveToParentViewController:nil];
                         [self.view removeFromSuperview];
                         [self removeFromParentViewController];
                         
                         _imageView.hidden = NO;
                         self.targetImageView.hidden = NO;
                         
                         if([delegate respondsToSelector:@selector(imageEditor:didDismissWithImageView:canceled:)]){
                             [delegate imageEditor:self didDismissWithImageView:self.targetImageView canceled:canceled];


                         }
                     }
     ];
}

#pragma mark- Properties

- (id<ImageEditorTransitionDelegate>)transitionDelegate
{
    if([self.delegate conformsToProtocol:@protocol(ImageEditorTransitionDelegate)]){
        return (id<ImageEditorTransitionDelegate>)self.delegate;
    }
    return nil;
}

- (void)setTitle:(NSString *)title
{
    [super setTitle:title];
    _toolInfo.title = title;
}

#pragma mark- ImageTool setting

+ (NSString*)defaultIconImagePath
{
    return nil;
}

+ (CGFloat)defaultDockedNumber
{
    return 0;
}

// Set the Title of the App ==========
+ (NSString*)defaultTitle
{
    return NSLocalizedString(@"Editor", @"");
    

}

+ (BOOL)isAvailable
{
    return true;
}

+ (NSArray*)subtools
{
    return [ImageToolInfo toolsWithToolClass:[ImageToolBase class]];
}

+ (NSDictionary*)optionalInfo
{
    return nil;
}




#pragma mark - SET MENU VIEW ================================

- (void)setMenuView {
    CGFloat x = 0;
    CGFloat W = 70;
    CGFloat H = _menuView.height;
    
    for(ImageToolInfo *info in self.toolInfo.sortedSubtools){
        if(!info.available){
            continue;
        }
        
        ToolbarMenuItem *view = [ImageEditorTheme menuItemWithFrame:CGRectMake(x, 0, W, H) target:self action:@selector(tappedMenuView:) toolInfo:info];
      
        [_menuView addSubview:view];
        x += W;
    }
    _menuView.contentSize = CGSizeMake(MAX(x, _menuView.frame.size.width+1), 0);
}

- (void)resetImageViewFrame  {
    
    CGSize size = (_imageView.image) ? _imageView.image.size : _imageView.frame.size;
  //  NSLog(@"size: %f - %f", size.width, size.height);

    if(size.width > 0   &&   size.height > 0) {
        CGFloat ratio = MIN(_scrollView.frame.size.width / size.width, _scrollView.frame.size.height / size.height);
        CGFloat W = ratio * size.width * _scrollView.zoomScale;
        CGFloat H = ratio * size.height * _scrollView.zoomScale - 20;
        _imageView.frame = CGRectMake((_scrollView.width-W)/2, (_scrollView.height-H)/2-5, W, H);
    }
    
    _scrollView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin);
    
    /*
    // CONSOLE LOGS
    NSLog(@"ImageVIEW: %f -- %f", _imageView.frame.size.width, _imageView.frame.size.height);
    NSLog(@"UIImage: %f -- %f", _imageView.image.size.width, _imageView.image.size.height);
    NSLog(@"scrollView: %f - %f", _scrollView.frame.size.width, _scrollView.frame.size.height);
    */
}

- (void)fixZoomScaleWithAnimated:(BOOL)animated {
    CGFloat minZoomScale = _scrollView.minimumZoomScale;
    _scrollView.maximumZoomScale = 0.95*minZoomScale;
    _scrollView.minimumZoomScale = 0.95*minZoomScale;
    [_scrollView setZoomScale:_scrollView.minimumZoomScale animated:animated];
}

- (void)resetZoomScaleWithAnimated:(BOOL)animated {
  //  NSLog(@"resetZoomScaleWithAnimated %f",_scrollView.zoomScale);
    _scrollView.zoomScale = 1;
    _scrollView.maximumZoomScale = 1.0;
    _scrollView.minimumZoomScale = 1.0;
    [_scrollView setZoomScale:_scrollView.minimumZoomScale animated:animated];
    
    NSLog(@"resetZoomScaleWithAnimated %f",_scrollView.zoomScale);
    
}

- (void)refreshImageView
{
    _imageView.image = _originalImage;
    
    [self resetImageViewFrame];
}

- (UIBarPosition)positionForBar:(id <UIBarPositioning>)bar
{
    return UIBarPositionTopAttached;
}

- (BOOL)shouldAutorotate
{
    return false;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}



#pragma mark- TOOL ACTIONS ===============
- (void)setCurrentTool:(ImageToolBase *)currentTool
{
    if (currentTool != _currentTool){
        [_currentTool cleanup];
        _currentTool = currentTool;
        [_currentTool setup];
        
        [self swapToolBarWithEditting:(_currentTool!=nil)];
        
        if(shouldShowFBAdd)
        {
            if([self.currentTool.toolInfo.toolName isEqualToString:@"StickerTool"] || [self.currentTool.toolInfo.toolName isEqualToString:@"FilterTool"] || [self.currentTool.toolInfo.toolName isEqualToString:@"TextTool"] || [self.currentTool.toolInfo.toolName isEqualToString:@"ColorizeTool"] )
            {
                
                [self.fbBannerView setHidden:NO];
                [self.bannerView setHidden:YES];
                [self.nativeAdView setHidden:YES];
            }
            else
            {
                [self.fbBannerView setHidden:YES];
                [self.bannerView setHidden:NO];
                [self.nativeAdView setHidden:YES];
                
                
            }

        }else
        {
            [self.fbBannerView setHidden:YES];
            [self.bannerView setHidden:NO];
            [self.nativeAdView setHidden:YES];
            
        }
        
        
    }
}



#pragma mark- MENU ACTIONS ==================
- (void)swapMenuViewWithEditting:(BOOL)editting
{
    [UIView animateWithDuration:kImageToolAnimationDuration animations:^{
            if (editting){
        _menuView.transform = CGAffineTransformMakeTranslation(0, self.view.height - _menuView.top);
        } else {
        _menuView.transform = CGAffineTransformIdentity;
        }
    }];
}

- (void)swapNavigationBarWithEditting:(BOOL)editting
{
    if(self.navigationController == nil){
        return;
    }
    
    [self.navigationController setNavigationBarHidden:editting animated:true];
    
    if (editting) {

        _navigationBar.hidden = NO;
        _navigationBar.transform = CGAffineTransformMakeTranslation(0, -_navigationBar.height);
        
        [UIView animateWithDuration:kImageToolAnimationDuration
                         animations:^{
        _navigationBar.transform = CGAffineTransformIdentity;
        }];
        
    } else {
        
        [UIView animateWithDuration:kImageToolAnimationDuration
                         animations:^{
                             _navigationBar.transform = CGAffineTransformMakeTranslation(0, -_navigationBar.height);
                         }
                         completion:^(BOOL finished) {
                             _navigationBar.hidden = YES;
                             _navigationBar.transform = CGAffineTransformIdentity;
                         }
         ];
    }
}

- (void)swapToolBarWithEditting:(BOOL)editting
{
    [self swapMenuViewWithEditting:editting];
    [self swapNavigationBarWithEditting:editting];
    
    if (self.currentTool){
        
        // Set the Title's font and color
        _navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
        [UIColor darkGrayColor], NSForegroundColorAttributeName,
        NAVBAR_FONT, NSFontAttributeName,nil];
        
        // Show the name of the current Tool you're using
        UINavigationItem *item  = [[UINavigationItem alloc] initWithTitle: self.currentTool.toolInfo.title];
        
        item.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"ImageEditor_OKBtnTitle", @"") style:UIBarButtonItemStylePlain target:self action:@selector(pushedDoneBtn:)];
        
        item.leftBarButtonItem  = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"ImageEditor_BackBtnTitle", @"") style:UIBarButtonItemStylePlain target:self action:@selector(pushedCancelBtn:)];
        
        
        // Set the button bar item's Tint Color and Attributes
        [item.rightBarButtonItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
        [UIColor darkGrayColor], NSForegroundColorAttributeName,
        NAVBAR_FONT, NSFontAttributeName,nil] forState:UIControlStateNormal];
        item.rightBarButtonItem.tintColor = [UIColor blackColor];

        
        [item.leftBarButtonItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
        [UIColor darkGrayColor], NSForegroundColorAttributeName,
        NAVBAR_FONT, NSFontAttributeName,nil] forState:UIControlStateNormal];
        item.leftBarButtonItem.tintColor = [UIColor whiteColor];

        
        [_navigationBar pushNavigationItem:item animated:(self.navigationController==nil)];
   
    } else {
        [_navigationBar popNavigationItemAnimated:(self.navigationController==nil)];
    }
}

- (void)setupToolWithToolInfo:(ImageToolInfo*)info
{
    if(self.currentTool){ return; }
    
    Class toolClass = NSClassFromString(info.toolName);
    
    if(toolClass){
        id instance = [toolClass alloc];
        if(instance!=nil && [instance isKindOfClass:[ImageToolBase class]]){
            instance = [instance initWithImageEditor:self withToolInfo:info];
            self.currentTool = instance;
        }
    }
}

- (void)tappedMenuView:(UITapGestureRecognizer*)sender
{
    UIView *view = sender.view;
    
    view.alpha = 0.2;
    [UIView animateWithDuration:kImageToolAnimationDuration
                     animations:^{
                         view.alpha = 1;
                     }];
    
    [self setupToolWithToolInfo:view.toolInfo];
    
}



#pragma mark - CANCEL BUTTON METHOD ===================
- (IBAction)pushedCancelBtn:(id)sender
{
    
    
    _imageView.image = _originalImage;
    
    self.currentTool = nil;
    [self resetImageViewFrame];
    
}

#pragma mark - DONE BUTTON METHOD ======================
- (IBAction)pushedDoneBtn:(id)sender
{
    
   // NSLog(@"self.currentTool %@",self.currentTool.toolInfo);
    self.view.userInteractionEnabled = NO;
    
  
    
    [self.currentTool executeWithCompletionBlock:^(UIImage *image, NSError *error, NSDictionary *userInfo) {
        if(error){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        else if(image){

            _originalImage = image;
            _imageView.image = image;
            
            [self resetImageViewFrame];
            if([self.currentTool.toolInfo.toolName isEqualToString:@"StickerTool"] || [self.currentTool.toolInfo.toolName isEqualToString:@"FilterTool"] || [self.currentTool.toolInfo.toolName isEqualToString:@"TextTool"] || [self.currentTool.toolInfo.toolName isEqualToString:@"ColorizeTool"])
            {
                if(shouldShowFBAdd)
                {
                    [self.fbInterstitialAd loadAd];

                }
                else
                {
                    if (self.interstitial.isReady) {
                        NSLog(@"Ad  ready");
                        [self.interstitial presentFromRootViewController:self];
                    } else {
                        NSLog(@"Ad wasn't ready");
                    }
                }
                
                
            }
       
            self.currentTool = nil;
            
        }
        self.view.userInteractionEnabled = true;
       }];
}



- (void)pushedCloseBtn:(id)sender  {
    
    if (self.targetImageView==nil){
        if([self.delegate respondsToSelector:@selector(imageEditorDidCancel:)]){
            [self.delegate imageEditorDidCancel:self];
      
        }
        else {
            [self dismissViewControllerAnimated:true completion:nil];
        }
        
    } else {
        _imageView.image = _targetImageView.image;
        [self restoreImageView:true];
    }
}

- (void)pushedFinishBtn:(id)sender
{
    NSLog(@"done button clicked..............");
    
    


    if(self.targetImageView==nil){
        
        if([self.delegate respondsToSelector:@selector(imageEditor:didFinishEdittingWithImage:)]){
            [self.delegate imageEditor:self didFinishEdittingWithImage:_originalImage];
        
            NSLog(@"..............");
            
        
        } else {
             NSLog(@"dismissViewControllerAnimated..............");
            [self dismissViewControllerAnimated:true completion:nil];
        }
        
    } else {
        
        
        _imageView.image = _originalImage;
        [self restoreImageView:NO];
    }
}



#pragma mark- ScrollView delegate ============================

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return _imageView;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    CGFloat Ws = _scrollView.frame.size.width - _scrollView.contentInset.left - _scrollView.contentInset.right;
    CGFloat Hs = _scrollView.frame.size.height - _scrollView.contentInset.top - _scrollView.contentInset.bottom;
    CGFloat W = _imageView.frame.size.width;
    CGFloat H = _imageView.frame.size.height;
    
    CGRect rct = _imageView.frame;
    rct.origin.x = MAX((Ws-W)/2, 0);
    rct.origin.y = MAX((Hs-H)/2, 0);
    _imageView.frame = rct;
}

#pragma mark - GADNativeExpressAdViewDelegate

- (void)nativeExpressAdViewDidReceiveAd:(GADNativeExpressAdView *)nativeExpressAdView {
    if (nativeExpressAdView.videoController.hasVideoContent) {
        NSLog(@"Received ad an with a video asset.");
    } else {
        NSLog(@"Received ad an without a video asset.");
    }
}

-(void)nativeExpressAdView:(GADNativeExpressAdView *)nativeExpressAdView didFailToReceiveAdWithError:(GADRequestError *)error
{
    NSLog(@"didFailToReceiveAdWithError with Error %@",error);
    nativeExpressAdView.hidden=YES;
}

#pragma mark - GADBannerViewDelegate

- (void)adViewDidReceiveAd:(GADBannerView *)bannerView {
    bannerView.hidden = NO;
}

- (void)adView:(GADBannerView *)adView didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"adView:didFailToReceiveAdWithError: %@", error.localizedDescription);
    [self.bannerView setHidden:YES];

}

- (GADInterstitial *)createAndLoadInterstitial {
    
    GADInterstitial *interstitial =
    [[GADInterstitial alloc] initWithAdUnitID:ADMOB_INTERSTITIAl_ID];
    interstitial.delegate = self;
    
    GADRequest *request = [GADRequest request];
    request.testDevices = @[ kGADSimulatorID, TEST_DEVICE_1,TEST_DEVICE_2,TEST_DEVICE_3 ];
    [interstitial loadRequest:request];
    //[interstitial loadRequest:[GADRequest request]];
    
    return interstitial;
    
    
}

- (void)interstitialDidDismissScreen:(GADInterstitial *)interstitial {
    self.interstitial = [self createAndLoadInterstitial];
}

#pragma mark - FBInterstitialViewDelegate


-(void) loadFBInterstitial
{
    self.fbInterstitialAd = [[FBInterstitialAd alloc] initWithPlacementID:FB_INTERSTITIAl_ID];
    self.fbInterstitialAd.delegate = self;
    // For auto play video ads, it's recommended to load the ad
    // at least 30 seconds before it is shown
   
}


// Now that you have added the code to load the ad, add the following functions
// to display the ad once it is loaded and to handle loading failures:
- (void)interstitialAdDidLoad:(FBInterstitialAd *)interstitialAd
{
    NSLog(@"Interstitial ad is loaded and ready to be displayed");
    
    // You can now display the full screen ad using this code:
    [interstitialAd showAdFromRootViewController:self];
}

- (void)interstitialAd:(FBInterstitialAd *)interstitialAd
      didFailWithError:(NSError *)error
{
    NSLog(@"Interstitial ad is failed to load with error: %@", error);
}

#pragma mark - FBBannerViewDelegate


- (void)adView:(FBAdView *)adView didFailWithError:(NSError *)error;
{
    NSLog(@"Ad failed to load");
    // Add code to hide the ad unit...
    // E.g. adView.hidden = YES;
    [self.fbBannerView setHidden:YES];
 
}

- (void)adViewDidLoad:(FBAdView *)adView;
{
    NSLog(@"Ad was loaded and ready to be displayed");
    // Add code to show the ad unit...
    // E.g. adView.hidden = NO;
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

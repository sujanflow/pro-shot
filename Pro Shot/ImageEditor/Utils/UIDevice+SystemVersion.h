/*============================
 
 Pro Shot
 
 iOS 7/8 iPhone Photo Editor App template
 created by FV iMAGINATION - 2014
 http://www.fvimagination.com
 
 ==============================*/



#import <UIKit/UIKit.h>

@interface UIDevice (SystemVersion)

+ (CGFloat)iosVersion;

@end

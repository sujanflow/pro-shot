/*============================
 
 Pro Shot
 
 iOS 7/8 iPhone Photo Editor App template
 created by FV iMAGINATION - 2014
 http://www.fvimagination.com
 
 ==============================*/


#import "FilterTool.h"
#import "FilterBase.h"
#import "Configs.h"
#import "Appirater.h"
#import "IAPController.h"

@interface FilterTool()

@end

@implementation FilterTool
{
    UIImage *_originalImage;
    UIScrollView *_menuScroll;
    
    // For IAP =========
    UIImageView *iapDot;
   // NSTimer *iapTimer;
    int tagINT,
    filterTAG;
    

}

+ (NSArray*)subtools
{
    return [ImageToolInfo toolsWithToolClass:[FilterBase class]];
}

+ (NSString*)defaultTitle
{
    return NSLocalizedString(@"FilterTool", @"");
}

+ (BOOL)isAvailable
{
    return true;
}

- (void)setup {
    
    _originalImage = self.editor.imageView.image;
    
    // Fire IAP timer
    //iapTimer = [NSTimer scheduledTimerWithTimeInterval:0.2  target:self selector:@selector(removeIapDots:)  userInfo:nil repeats:true];
    tagINT = 0;
    filterTAG = -1;
    
    
    _menuScroll = [[UIScrollView alloc] initWithFrame:self.editor.menuView.frame];
    _menuScroll.backgroundColor = self.editor.menuView.backgroundColor;
    _menuScroll.showsHorizontalScrollIndicator = NO;
    [self.editor.view addSubview:_menuScroll];
    
    // Fire IAP timer
    
    tagINT = 0;
    
    
    [self setFilterMenu];
    
    _menuScroll.transform = CGAffineTransformMakeTranslation(0, self.editor.view.height-_menuScroll.top);
    [UIView animateWithDuration:kImageToolAnimationDuration animations:^{
        _menuScroll.transform = CGAffineTransformIdentity;
    }];
}

- (void)cleanup {
   //  [iapTimer invalidate];
    
    [UIView animateWithDuration:kImageToolAnimationDuration animations:^{
        _menuScroll.transform = CGAffineTransformMakeTranslation(0, self.editor.view.height-_menuScroll.top);
    } completion:^(BOOL finished) {
        [_menuScroll removeFromSuperview];
    }];
}

#pragma mark - REMOVE IAP DOTS METHOD  =================
-(void)refreshViewAfterInApp
{
    NSLog(@"refreshViewAfterInApp");
    [self removeIapDots];
    if(isAdRemoved)
        _menuScroll.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height-55 , [UIScreen mainScreen].bounds.size.width , 55);
    
}
// Remove iapDots icons from items that have been purchased with IAP
-(void)removeIapDots {
    if (iapForEffects || iapMade) {
        for (int i = 300+freeFilters; i <= iapDot.tag; i++) {
            [[self.editor.view viewWithTag:i] removeFromSuperview];
        }
       // [iapTimer invalidate];
    }
    // NSLog(@"timerON!");
}

- (void)executeWithCompletionBlock:(void (^)(UIImage *, NSError *, NSDictionary *))completionBlock {
    completionBlock(self.editor.imageView.image, nil, nil);
}



#pragma mark - SETUP FILTERS MENU ================================

- (void)setFilterMenu {
    
    CGFloat W = 70;
    CGFloat x = 0;
    
    UIImage *iconThumnail = [_originalImage aspectFill:CGSizeMake(50, 50)];
    
    for(ImageToolInfo *info in self.toolInfo.sortedSubtools){
        if(!info.available){
            continue;
        }
        
        ToolbarMenuItem *item = [ImageEditorTheme menuItemWithFrame:CGRectMake(x, 0, W, _menuScroll.height) target:self action:@selector(tappedFilterPanel:) toolInfo:info];
        tagINT++;
        item.tag = tagINT;
        
        // Add a little circle on the top of the PAID items that need to be unlocked with IAP
        if (!iapForEffects && !iapMade && item.tag >= freeFrames) {
            iapDot = [[UIImageView alloc]initWithFrame:CGRectMake(0, 10, 6, 6)];
            iapDot.backgroundColor = PURPLE_COLOR;
            iapDot.layer.cornerRadius = iapDot.bounds.size.width/2;
            iapDot.tag = tagINT+600;
            [item addSubview:iapDot];
            //NSLog(@"iapDot TAG: %ld", (long)iapDot.tag);
        }
        //====================================================================
        [_menuScroll addSubview:item];
        //item.userInfo = @{@"filePath" : filePath};
        x += W;
        
        if(item.iconImage==nil){
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                UIImage *iconImage = [self filteredImage:iconThumnail withToolInfo:info];
                [item performSelectorOnMainThread:@selector(setIconImage:) withObject:iconImage waitUntilDone:NO];
            });
        }
    }
    _menuScroll.contentSize = CGSizeMake(MAX(x, _menuScroll.frame.size.width+1), 0);
}

- (void)tappedFilterPanel:(UITapGestureRecognizer*)sender
{
    UIView *view = sender.view;
    NSLog(@"tag: %ld", (long)view.tag);
    view.alpha = 0.2;
    [UIView animateWithDuration:kImageToolAnimationDuration
                     animations:^{
                         view.alpha = 1;
                     }
     ];
    
    if (!iapForEffects && !iapMade && view.tag >= freeFilters) {
        IAPController *iapVC = [[IAPController alloc]initWithNibName:@"IAPController" bundle:nil];
        iapVC.delegate=self.editor;
        
        [self.editor presentViewController: iapVC animated:true completion:nil];
        
        
        
        /*========================================================================================
         IAP MADE!
         =========================================================================================*/
    } else {
        
      
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            UIImage *image = [self filteredImage:_originalImage withToolInfo:view.toolInfo];
            [self.editor.imageView performSelectorOnMainThread:@selector(setImage:) withObject:image waitUntilDone:NO];
        });
        
        filterTAG++;
        view.tag = filterTAG;
    }
    
    

    
}

- (UIImage*)filteredImage:(UIImage*)image withToolInfo:(ImageToolInfo*)info
{
    @autoreleasepool {
        Class filterClass = NSClassFromString(info.toolName);
        if([(Class)filterClass conformsToProtocol:@protocol(FilterBaseProtocol)]){
            return [filterClass applyFilter:image];
        }
        return nil;
    }
}

@end

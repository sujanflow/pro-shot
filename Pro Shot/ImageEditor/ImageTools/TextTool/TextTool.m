

#import "TextTool.h"

#import "CircleView.h"
#import "ColorPickerView.h"
#import "FontPickerView.h"
#import "TextViewEdit.h"
#import "TextSettingView.h"
#import "Configs.h"
#import "FontCollectionViewCell.h"

#import "IAPController.h"

//#import "Appirater.h"
//#import "iRate.h"

static NSString *const TextViewActiveViewDidChangeNotification = @"TextViewActiveViewDidChangeNotificationString";
static NSString *const TextViewActiveViewDidTapNotification = @"TextViewActiveViewDidTapNotificationString";


@interface _TextView : UIView
<
UITextViewDelegate,
UITextFieldDelegate,
TextSettingViewDelegate
>
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) UIFont *font;
@property (nonatomic, strong) UIColor *fillColor;
@property (nonatomic, strong) UIColor *borderColor;
@property (nonatomic, assign) CGFloat borderWidth;
@property (nonatomic, assign) NSTextAlignment textAlignment;

+ (void)setActiveTextView:(_TextView *)view;
- (void)setScale:(CGFloat)scale;
- (void)sizeToFitWithMaxWidth:(CGFloat)width lineHeight:(CGFloat)lineHeight;
- (void) tiltDegrees:(CGFloat)degrees atXaxis:(BOOL)isXaxis;

@end



@interface ToolbarMenuItem(Private)
- (UIImageView*)iconView;
@end

@implementation ToolbarMenuItem(Private)
- (UIImageView*)iconView {
    return _iconView;
}
@end



@interface TextTool()
<
ColorPickerViewDelegate,
FontPickerViewDelegate,
UITextViewDelegate,
UITextFieldDelegate,
TextSettingViewDelegate,
UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource
>
@property (nonatomic, strong) _TextView *selectedTextView;
@end

@implementation TextTool
{
    UIImage *_originalImage;
    UIImage *_thumnailImage;
    UIView *_workingView;
    UIScrollView *_menuScroll;

    UIActivityIndicatorView *_indicatorView;

    TextSettingView *_settingView;
    
    
    // Toolbar Buttons ===========
    ToolbarMenuItem *_textBtn;
    ToolbarMenuItem *_colorBtn;
    ToolbarMenuItem *_fontBtn;
    ToolbarMenuItem *_3DBtn;
    
    
    ToolbarMenuItem *_eraserBtn;
    
    ToolbarMenuItem *_alignLeftBtn;
    ToolbarMenuItem *_alignCenterBtn;
    ToolbarMenuItem *_alignRightBtn;
    
    
    UIView *_sliderView;
    UISlider *_opacitySlider;
    UISlider *_dropShadowSlider;
    
    UIView *_perspectiveOptionView;
    ToolbarMenuItem *noneButt;
    ToolbarMenuItem *left3dButt;
    ToolbarMenuItem *right3dButt;
    ToolbarMenuItem *top3dButt;
    ToolbarMenuItem *bottom3dButt;
    
    
    NSArray *fontList;
    UITableView *fontTableView;
    UIButton *okButton;
    
    UICollectionView *fontCollectionView;
    
    NSArray *colorsArray;
    UIScrollView *colorScrollView;
    UIView *colorPickerView;
    UIButton *colorButt;
    int colorTag;

    NSString *fontStr;
    
}


+ (NSArray*)subtools {
    return nil;
}

+ (NSString*)defaultTitle
{
    return NSLocalizedString(@"TextTool", @"");
}

+ (BOOL)isAvailable
{
    return true;
}




#pragma mark- TEXT TOOL INIT ==============
- (void)setup {
    
    _originalImage = self.editor.imageView.image;
    _thumnailImage = _originalImage;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(activeTextViewDidChange:) name:TextViewActiveViewDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(activeTextViewDidTap:) name:TextViewActiveViewDidTapNotification object:nil];
    
    
    // Toolbar ScrollView =======
    _menuScroll = [[UIScrollView alloc] initWithFrame:self.editor.menuView.frame];
    _menuScroll.backgroundColor = self.editor.menuView.backgroundColor;
    _menuScroll.showsHorizontalScrollIndicator = NO;
    [self.editor.view addSubview:_menuScroll];
    
    
    // Working View for merging images + text
    _workingView = [[UIView alloc] initWithFrame:[self.editor.view convertRect:self.editor.imageView.frame fromView:self.editor.imageView.superview]];
    _workingView.clipsToBounds = true;
    [self.editor.view addSubview:_workingView];
    
    [_workingView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)]];

    
    // Settings View (for delegates about chaing Text and Font)
    _settingView = [[TextSettingView alloc] init];
    _settingView.delegate = self;
    [self.editor.view addSubview:_settingView];
    
    
    _perspectiveOptionView = [[UIView alloc] initWithFrame:CGRectMake(0,self.editor.menuView.frame.origin.y-self.editor.menuView.frame.size.height, self.editor.menuView.frame.size.width,self.editor.menuView.frame.size.height)];
    
    _perspectiveOptionView.backgroundColor = [MENU_COLOR colorWithAlphaComponent:1.0];
    [self.editor.view addSubview:_perspectiveOptionView];
    
    
    
    _sliderView = [[UIView alloc] initWithFrame:CGRectMake(0,self.editor.menuView.frame.origin.y-self.editor.menuView.frame.size.height, self.editor.menuView.frame.size.width,self.editor.menuView.frame.size.height)];
    _sliderView.backgroundColor = [MENU_COLOR colorWithAlphaComponent:1.0];
    [self.editor.view addSubview:_sliderView];
    
   
    [self setup3DOptionView];
    
    [self setupSlider];
    
    
    /*  FONT TABLEVIEW INIT =========*/
    fontList = @[
                 @"Always Together",
                 @"BlockFont",
                 @"Bukhari Script",
                 @"FredokaOne-Regular",
                 @"FrontageCondensed-Outline",
                 @"Galano Grotesque Demo",
                 @"RazedTrend",
                 @"HelloStockholm-Regular",
                 @"Humblle Rought All Caps",
                 @"King Basil Lite",
                 @"LAIKA",
                 @"MadrasExtraBoldItalic",
                 @"MadrasExtraLight",
                 @"makhina",
                 @"Manoyri-Regular",
                 @"Modeka",
                 @"MONARC SANS",
                 @"mustardo",
                 @"NulshockRg-Bold",
                 @"OldGrowth-Regular",
                 @"Olegos",
                 @"RechargeRg-Bold",
                 @"Rimbo-Regular",
                 @"Simpla",
                 @"Skywalker free demo",
                 @"Smoothie Shoppe",
                 @"Supermolot Light",
                 @"Taurus Mono Outline",
                 @"UniNeueLight",
                 @"Vagtur",
                 @"Verbena",
                 @"Womby-Regular",
                 
                  ];
    
    CGRect tableRect = CGRectMake(0, self.editor.view.frame.size.height, self.editor.view.frame.size.width, 180);
    fontTableView = [[UITableView alloc]initWithFrame: tableRect style:UITableViewStylePlain];
    
    fontTableView.backgroundColor = [UIColor blackColor];
    fontTableView.rowHeight = 50;
    fontTableView.scrollEnabled = true;
    fontTableView.showsVerticalScrollIndicator = true;
    fontTableView.userInteractionEnabled = true;
    fontTableView.bounces = true;

    fontTableView.delegate = self;
    fontTableView.dataSource = self;
   // NSLog(@"FONTS LIST:  %@", fontList);
    
   // [self.editor.view addSubview:fontTableView];
    
    /* END FONT TABLEVIEW =============================*/
    
    /* Start FONT CollectionView =============================*/
    
    UICollectionViewFlowLayout *collectionViewFlowLayout = [[UICollectionViewFlowLayout alloc] init];
    [collectionViewFlowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    collectionViewFlowLayout.minimumLineSpacing = 0.0;
    collectionViewFlowLayout.minimumInteritemSpacing = 10.0;
    
    
    CGRect viewRect = CGRectMake(0, self.editor.view.frame.size.height, self.editor.view.frame.size.width, 70);
    fontCollectionView = [[UICollectionView alloc]initWithFrame:viewRect collectionViewLayout:collectionViewFlowLayout];
    
    fontCollectionView.backgroundColor = [UIColor whiteColor];
    fontCollectionView.scrollEnabled = true;
    fontCollectionView.showsVerticalScrollIndicator = NO;
    fontCollectionView.userInteractionEnabled = true;
    fontCollectionView.bounces = true;
    
    fontCollectionView.delegate = self;
    fontCollectionView.dataSource = self;
    [fontCollectionView registerClass:[FontCollectionViewCell class]
            forCellWithReuseIdentifier:@"Cell"];

    // NSLog(@"FONTS LIST:  %@", fontList);
    
    [self.editor.view addSubview:fontCollectionView];
    
    
    /* END FONT CollectionView =============================*/
    
    
    
    /* COLOR PICKER VIEW ==============================*/
    colorPickerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.editor.view.frame.size.height, self.editor.view.frame.size.width, 50)];
    colorPickerView.clipsToBounds = true;
    colorPickerView.backgroundColor = [UIColor lightGrayColor];
    [self.editor.view addSubview:colorPickerView];
    
    // ScrollView for Color Buttons =============
    colorScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, colorPickerView.frame.size.width, colorPickerView.frame.size.height)];
    [colorScrollView setBackgroundColor: [UIColor clearColor]];
    colorScrollView.scrollEnabled = true;
    colorScrollView.userInteractionEnabled = true;
    colorScrollView.showsHorizontalScrollIndicator = true;
    colorScrollView.showsVerticalScrollIndicator = false;
    [colorPickerView addSubview:colorScrollView];

    [self setColorsAndButtons];
    
    /* END COLOR PICKER VIEW ================*/

    
    
    [self setMenu];
    
    self.selectedTextView = nil;
    
    [self addNewText];
    
    _menuScroll.transform = CGAffineTransformMakeTranslation(0, self.editor.view.height-_menuScroll.top);
    [UIView animateWithDuration:kImageToolAnimationDuration
    animations:^{
        _menuScroll.transform = CGAffineTransformIdentity;
    }];
    
}

- (void)setupSlider {
   
    
    _opacitySlider = [self sliderWithValue:1.0 minimumValue:0 maximumValue:1 withTitle:NSLocalizedString(@"OPACITY", @"") action:@selector(sliderDidChange:)];
    _opacitySlider.superview.center = CGPointMake(self.editor.view.width/4,_opacitySlider.frame.size.height/2);
    _opacitySlider.thumbTintColor = [UIColor whiteColor];
    _opacitySlider.minimumTrackTintColor = [UIColor darkGrayColor];
    _opacitySlider.maximumTrackTintColor = LIGHT_COLOR;
    
    _dropShadowSlider = [self sliderWithValue:1.0 minimumValue:0 maximumValue:1 withTitle:NSLocalizedString(@"SHADOW", @"") action:@selector(sliderDidChangeShadow:)];
    _dropShadowSlider.superview.center = CGPointMake((self.editor.view.width/4)*3,_dropShadowSlider.frame.size.height/2);
    _dropShadowSlider.thumbTintColor = [UIColor whiteColor];
    _dropShadowSlider.minimumTrackTintColor = [UIColor darkGrayColor];
    _dropShadowSlider.maximumTrackTintColor = LIGHT_COLOR;
    
    
    
}

-(void)setup3DOptionView
{
    int xCoord = 5;
    int yCoord = 0;
    int buttonWidth;
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
         buttonWidth = _perspectiveOptionView.frame.size.height;

    }else{
        
        buttonWidth = (_perspectiveOptionView.frame.size.width-10*5)/5;
    }
    int buttonHeight= _perspectiveOptionView.frame.size.height;
    
    int gapBetweenButtons = 10;

    NSArray *_menu = @[
                       @{@"title":NSLocalizedString(@"None", @""),
                         @"icon":[UIImage imageNamed: @"NonePerspective"]
                         },
                       @{@"title":NSLocalizedString(@"Left ", @""),
                         @"icon":[UIImage imageNamed: @"LeftPerspective"]
                         },
                       
                       @{@"title":NSLocalizedString(@"Right", @""),
                         @"icon":[UIImage imageNamed:@"RightPerspective"]
                         },
                       
                       @{@"title":NSLocalizedString(@"Top", @""),
                         @"icon":[UIImage imageNamed:@"TopPerspective"]
                         },
                       
                       @{@"title":NSLocalizedString(@"Bottom", @""),
                         @"icon":[UIImage imageNamed:@"BottomPerspective"]
                         },
                       ];
    
    NSInteger tag = 0;
    
    for(NSDictionary *obj in _menu)   {
        ToolbarMenuItem *view = [ImageEditorTheme menuItemWithFrame:CGRectMake(xCoord, yCoord, buttonWidth, buttonHeight) target:self action:@selector(tapped3DPanel:) toolInfo:nil];
        view.tag = tag++;
        view.title = obj[@"title"];
        view.iconImage = obj[@"icon"];
        
        
        switch (view.tag) {
            case 1:
                noneButt=view;
                break;
            case 2:
                left3dButt = view;
                break;
                
            case 3:
                right3dButt = view;
                break;
            case 4:
                top3dButt = view;
                break;
            case 5:
                bottom3dButt = view;
                break;
    
        }
        
        [_perspectiveOptionView addSubview:view];
        xCoord += buttonWidth + gapBetweenButtons;

    }

}


-(void)setColorsAndButtons {
    
     // Color Buttons & Colors ===================
     colorsArray = [NSArray arrayWithObjects:
    [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0],
    [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0],

    [UIColor colorWithRed:237.0/255.0 green:85.0/255.0 blue:100.0/255.0 alpha:1.0],
    [UIColor colorWithRed:218.0/255.0 green:69.0/255.0 blue:83.0/255.0 alpha:1.0],
    [UIColor colorWithRed:251.0/255.0 green:110.0/255.0 blue:82.0/255.0 alpha:1.0],
    [UIColor colorWithRed:246.0/255.0 green:187.0/255.0 blue:67.0/255.0 alpha:1.0],
    [UIColor colorWithRed:160.0/255.0 green:212.0/255.0 blue:104.0/255.0 alpha:1.0],
    [UIColor colorWithRed:140.0/255.0 green:192.0/255.0 blue:81.0/255.0 alpha:1.0],
    [UIColor colorWithRed:69.0/255.0 green:208.0/255.0 blue:175.0/255.0 alpha:1.0],
    [UIColor colorWithRed:79.0/255.0 green:192.0/255.0 blue:232.0/255.0 alpha:1.0],
    [UIColor colorWithRed:93.0/255.0 green:155.0/255.0 blue:236.0/255.0 alpha:1.0],
    [UIColor colorWithRed:150.0/255.0 green:123.0/255.0 blue:220.0/255.0 alpha:1.0],
    [UIColor colorWithRed:236.0/255.0 green:136.0/255.0 blue:192.0/255.0 alpha:1.0],
    [UIColor colorWithRed:230.0/255.0 green:233.0/255.0 blue:238.0/255.0 alpha:1.0],
    [UIColor colorWithRed:101.0/255.0 green:109.0/255.0 blue:120.0/255.0 alpha:1.0],

    // You can add new UIColors here....
                    
                    
    nil];
    
    
     int xCoord = 0;
     int yCoord = 0;
     int buttonWidth = 50;
     int buttonHeight= colorPickerView.frame.size.height;
     int gapBetweenButtons = 0;
     
     // Loop for creating buttons
     for (int i = 0; i < colorsArray.count; i++) {
     colorButt = [UIButton buttonWithType:UIButtonTypeCustom];
     colorButt.frame = CGRectMake(xCoord, yCoord, buttonWidth,buttonHeight);
     colorButt.tag = i;
     [colorButt setBackgroundColor: [colorsArray objectAtIndex:i]];
     [colorButt addTarget:self action:@selector(colorButtTapped:) forControlEvents:UIControlEventTouchUpInside];
     [colorScrollView addSubview:colorButt];
     
     xCoord += buttonWidth + gapBetweenButtons;
     }
    
    colorScrollView.contentSize = CGSizeMake(buttonWidth * colorsArray.count +1, yCoord);
     
}


#pragma mark - COLOR BUTTONS METHOD ====================
-(void)colorButtTapped: (UIButton *)sender {
    _txtView.textColor = [colorsArray objectAtIndex: sender.tag];
    
    _settingView.selectedFillColor = [colorsArray objectAtIndex: sender.tag];
   
    _colorBtn.iconView.backgroundColor = [UIColor clearColor];

}


#pragma mark - FONT Collectionview DELEGATES ============
// Calculate number of sections
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

// Every section has to have every cell filled, as we need to add empty cells as well to correct the spacing
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [fontList count];
}

// And now the most important one
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
   
//    UICollectionViewCell* cell = [fontCollectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([UICollectionViewCell class]) forIndexPath:indexPath];
//    
//    if (!cell)
//    {
//        cell  = [fontCollectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([UICollectionViewCell class]) forIndexPath:indexPath];
//        
//        cell.contentView.width = 100;
//        cell.contentView.backgroundColor = [UIColor whiteColor];
//        
//    }
    
    FontCollectionViewCell *cell = [fontCollectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    
    NSString *fontStr1 = [fontList objectAtIndex:indexPath.row];
//    cell.label.text = [fontList objectAtIndex:indexPath.row];
//    
//    cell.label.font = [UIFont fontWithName:fontStr1 size:17];
//    cell.backgroundColor = [UIColor clearColor];
//    cell.label.textColor = [UIColor blackColor];
//
    cell.imageView.image=nil;
    NSString* filePath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"Fonts Preview/%@",fontStr1] ofType:@"png"];
    
   // NSLog(@"filePath before %@",filePath);
    if(!filePath.length)
    {
        filePath= [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"Fonts Preview/%@",fontStr1] ofType:@"jpg"];
    }
   // NSLog(@"filePath after %@",filePath);
    
    cell.imageView.image=[UIImage imageWithContentsOfFile:filePath];
    
    if(!iapForFonts && !iapMade && indexPath.item >= freeFonts-1)
    {
        //add;
        [cell.iapDot setHidden:NO];

    }
    else
    {
        [cell.iapDot setHidden:YES];

    }
    
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
    //NSLog(@"cell size %lf for index %ld",cell.frame.size.width,(long)indexPath.row);
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
//    NSString *fontStr1 = [fontList objectAtIndex:indexPath.row];
// 
//    CGSize constraint = CGSizeMake(CGFLOAT_MAX, 50);
//    CGSize size;
//    
//    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
//    
//    CGSize boundingBox = [fontStr1 boundingRectWithSize:constraint
//                                                options:NSStringDrawingUsesLineFragmentOrigin
//                                             attributes:@{NSFontAttributeName:[UIFont fontWithName:fontStr1 size:17]}
//                                                context:context].size;
//    
//    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
//    
//    
//    //NSLog(@"size %lf",size.width);
//    return CGSizeMake(size.width+10, 50);
    
    return CGSizeMake(70, 70);
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    fontStr = [fontList objectAtIndex: indexPath.row];
    
    if(!iapForFonts &&  !iapMade && indexPath.item >= freeFonts-1)
    {
        IAPController *iapVC = [[IAPController alloc]initWithNibName:@"IAPController" bundle:nil];
        iapVC.delegate=self.editor;
        [self.editor presentViewController: iapVC animated:true completion:nil];
        
        /*========================================================================================
         IAP MADE!
         =========================================================================================*/

    }
    else
    {
        _selectedTextView.font = [UIFont fontWithName:fontStr size:_selectedTextView.font.pointSize];
        _settingView.selectedFont = _selectedTextView.font;
        [self textSettingView:_settingView didChangeFont:_selectedTextView.font];
        
    }
    
//    int useCount=[[[NSUserDefaults standardUserDefaults] valueForKey:fontStr] intValue];
    
//    if(indexPath.row>4 && ![[NSUserDefaults standardUserDefaults] boolForKey:@"kAppiraterRatedCurrentVersion"])
//    {
//
//        if(useCount)
//        {
//
//            useCount++;
//            [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInt:useCount] forKey:fontStr];
//
//            if (useCount>=freeTextUsage) {
//
////                [Appirater setAppId:APP_ITUNES_ID];
////
////                [Appirater setDaysUntilPrompt:1];
////                [Appirater setUsesUntilPrompt:0];
////                [Appirater setSignificantEventsUntilPrompt:-1];
////                [Appirater setTimeBeforeReminding:3];
////                [Appirater setDebug:YES];
////                [[Appirater sharedInstance] setDelegate:self];
////
////                [Appirater appLaunched:YES];
//
//                //                if([SKStoreReviewController class]){
//                //                    [SKStoreReviewController requestReview] ;
//                //                }
//
//                [iRate sharedInstance].appStoreCountry=@"US";
//                [[iRate sharedInstance] setAppStoreID:[APP_ITUNES_ID integerValue]];
//
//                [iRate sharedInstance].onlyPromptIfLatestVersion = NO;
//
//                [iRate sharedInstance].daysUntilPrompt = freeDaysForRating;
//                [iRate sharedInstance].usesUntilPrompt = 0;
//                [iRate sharedInstance].eventsUntilPrompt = 0;
//                [iRate sharedInstance].remindPeriod = 0;
//
//                [iRate sharedInstance].delegate = self;
//                [[iRate sharedInstance] logEvent:NO];
//            }
//            else
//            {
//                _selectedTextView.font = [UIFont fontWithName:fontStr size:_selectedTextView.font.pointSize];
//                 _settingView.selectedFont = _selectedTextView.font;
//                [self textSettingView:_settingView didChangeFont:_selectedTextView.font];
//
//            }
//
//        }
//        else
//        {
//            [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInt:1] forKey:fontStr];
//            _selectedTextView.font = [UIFont fontWithName:fontStr size:_selectedTextView.font.pointSize];
//            _settingView.selectedFont = _selectedTextView.font;
//
//            [self textSettingView:_settingView didChangeFont:_selectedTextView.font];
//
//        }
//
//    }
//    else
//    {
//        _selectedTextView.font = [UIFont fontWithName:fontStr size:_selectedTextView.font.pointSize];
//         _settingView.selectedFont = _selectedTextView.font;
//        [self textSettingView:_settingView didChangeFont:_selectedTextView.font];
//
//    }

//    _selectedTextView.font = [UIFont fontWithName:fontStr size:_selectedTextView.font.pointSize];
//    _settingView.selectedFont = _selectedTextView.font;
//    [self textSettingView:_settingView didChangeFont:_selectedTextView.font];
    
    
}



//#pragma mark iRate delegate methods
//
//- (void)iRateUserDidRequestReminderToRateApp
//{
//    //reset event count after every 5 (for demo purposes)
//    NSLog(@"iRateUserDidRequestReminderToRateApp");
//}
//
//-(void)iRateUserDidDeclineToRateApp
//{
//    NSLog(@"iRateUserDidDeclineToRateApp");
//    [self.editor.imageView setImage:_originalImage];
//}
//
//-(void)iRateUserDidAttemptToRateApp
//{
//    NSLog(@"iRateUserDidAttemptToRateApp");
//}
//
//-(void)iRateDidPromptForRating
//{
//    NSLog(@"iRateDidPromptForRating");
//    _selectedTextView.font = [UIFont fontWithName:fontStr size:_selectedTextView.font.pointSize];
//    [self textSettingView:_settingView didChangeFont:_selectedTextView.font];
//
//}

//#pragma mark- Appirater delegate
//
//-(void)appiraterDidDeclineToRate:(Appirater *)appirater
//{
//    NSLog(@"appiraterDidDeclineToRate in text");
//
//}
//
//-(void)appiraterDidOptToRate:(Appirater *)appirater
//{
//     NSLog(@"appiraterDidOptToRate in text fontStr %@",fontStr);
//    _selectedTextView.font = [UIFont fontWithName:fontStr size:_selectedTextView.font.pointSize];
//    [self textSettingView:_settingView didChangeFont:_selectedTextView.font];
//
//}



#pragma mark - FONT TABLEVIEW DELEGATES ============
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section  {
    return [fontList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    NSString *fontStr1 = [fontList objectAtIndex:indexPath.row];
    cell.textLabel.text = [fontList objectAtIndex:indexPath.row];
    
    cell.textLabel.font = [UIFont fontWithName:fontStr1 size:17];
    cell.backgroundColor = [UIColor blackColor];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Set Font of the _txtView =====
    fontStr = [fontList objectAtIndex: indexPath.row];

    _selectedTextView.font = [UIFont fontWithName:fontStr size:_selectedTextView.font.pointSize];
  //  _settingView.selectedFont = _selectedTextView.font;
    [self textSettingView:_settingView didChangeFont:_selectedTextView.font];
    
}





#pragma mark - TOOBAL MENU BUTTONS - SETUP ======================
- (void)setMenu  {
    
    CGFloat W = 70;
    CGFloat H = _menuScroll.height;
    CGFloat x = 0;
    
    NSArray *_menu = @[
                       @{@"title":NSLocalizedString(@"TextTool_MenuItemNew", @""),
                         @"icon":[UIImage imageNamed: @"ttAddTextButt"]
                        },
                       
                       @{@"title":NSLocalizedString(@"TextTool_MenuItemColor", @""),
                         @"icon":[UIImage imageNamed:@"ttColorButt"]
                         },
                       
                       @{@"title":NSLocalizedString(@"TextTool_MenuItemFont", @""),
                         @"icon":[UIImage imageNamed:@"ttFontButt"]
                         },
                       @{@"title":NSLocalizedString(@"TextTool_MenuItem3D", @""),
                         @"icon":[UIImage imageNamed:@"tt3Dbutton"]
                         },
                       
                       @{@"title":NSLocalizedString(@"TextTool_MenuItemAlignLeft", @""),
                         @"icon":[UIImage imageNamed:@"ttAlLeft"]
                         },
                       @{@"title":NSLocalizedString(@"TextTool_MenuItemAlignCenter", @""),
                         @"icon":[UIImage imageNamed:@"ttAlCenter"]
                         },
                       @{@"title":NSLocalizedString(@"TextTool_MenuItemAlignRight", @""),
                         @"icon":[UIImage imageNamed:@"ttAlRight"]
                         },
                        
                       ];
    
    NSInteger tag = 0;
    
    for(NSDictionary *obj in _menu)   {
        ToolbarMenuItem *view = [ImageEditorTheme menuItemWithFrame:CGRectMake(x, 0, W, H) target:self action:@selector(tappedMenuPanel:) toolInfo:nil];
        view.tag = tag++;
        view.title = obj[@"title"];
        view.iconImage = obj[@"icon"];
        
        
        switch (view.tag) {
            case 1:
                _colorBtn = view;
                break;
                
            case 2:
                _fontBtn = view;
                break;
            case 3:
                _3DBtn = view;
                break;
                
            // Alignment buttons ========
            case 4:
                _alignLeftBtn = view;
                break;
            case 5:
                _alignCenterBtn = view;
                break;
            case 6:
                _alignRightBtn = view;
                break;
                
        }
        
        [_menuScroll addSubview:view];
        x += W;
    }
    _menuScroll.contentSize = CGSizeMake(MAX(x, _menuScroll.frame.size.width+1), 0);
}


#pragma mark - TOOLBAR MENU BUTTONS - METHODS ================
- (void)tappedMenuPanel:(UITapGestureRecognizer*)sender
{
    UIView *view = sender.view;
    
    switch (view.tag) {
        case 0: {
          
            [self addNewText];
            //[self hideColorPickerView];
            [self hideColorPickerView];
            [self hideSliderView];
            [self hide3DView];
            
            [self showFontTableView];
            
            break;
        }
        case 1:
            // Color Button action =============
            [self hideFontTableView];
            [self hideSliderView];
            [self hide3DView];
            
            [self showColorPickerView];
            break;
            
        case 2:
            // Fonts Button action ============
            [self showFontTableView];
            
            [self hideSliderView];
            [self hideColorPickerView];
            [self hide3DView];
            
            break;
        case 3:
            // 3D Button action ============
            [self hideFontTableView];
            [self hideSliderView];
            [self hideColorPickerView];
            
            [self show3DView];
            break;


            // Text Alignment actions ==========
        case 4:
            [self setTextAlignment:NSTextAlignmentLeft];
            break;
        case 5:
            [self setTextAlignment:NSTextAlignmentCenter];
            break;
        case 6:
            [self setTextAlignment:NSTextAlignmentRight];
            break;
            }
    
    view.alpha = 0.2;
    [UIView animateWithDuration:kImageToolAnimationDuration
        animations:^{
        view.alpha = 1;
    }];
    
    
}

#pragma mark - 3DBAR BUTTONS - METHODS ================
- (void)tapped3DPanel:(UITapGestureRecognizer*)sender
{
    UIView *view = sender.view;
    
    switch (view.tag) {
        case 0: {
            CATransform3D aTransform = CATransform3DIdentity;
            _selectedTextView.layer.transform = aTransform;

            break;
        }
        case 1:
            [_selectedTextView tiltDegrees:-30.f atXaxis:YES];
            
            break;
            
        case 2:
            [_selectedTextView tiltDegrees:30.f atXaxis:YES];
            
            break;
        case 3:
            [_selectedTextView tiltDegrees:30.f atXaxis:NO];
            
            break;
        case 4:
            [_selectedTextView tiltDegrees:-30.f atXaxis:NO];
            
            break;
            
    }
    
    view.alpha = 0.2;
    [UIView animateWithDuration:kImageToolAnimationDuration
                     animations:^{
                         view.alpha = 1;
                     }];
    
    
}




#pragma mark - SHOW / HIDE FONT TABLEVIEW ======================
-(void)showFontTableView {
   
    [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^ {
        CGRect ftbFrame = fontCollectionView.frame;
        ftbFrame.origin.y = _menuScroll.top - fontCollectionView.frame.size.height;
        fontCollectionView.frame = ftbFrame;
        
        } completion:^(BOOL finished) {
        
           
            okButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
            okButton.tintColor=[UIColor darkGrayColor];
            okButton.titleLabel.textColor=[UIColor darkGrayColor];
            
            NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] init];
            [attString appendAttributedString:[[NSAttributedString alloc] initWithString:@"OK" attributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                             [UIColor darkGrayColor], NSForegroundColorAttributeName,
                                                                                                             NAVBAR_FONT, NSFontAttributeName,nil]]];
            
            [okButton setAttributedTitle:attString forState:UIControlStateNormal];
            
            okButton.backgroundColor=[UIColor whiteColor];
            //okButton.frame = CGRectMake(fontCollectionView.frame.size.width -32, fontCollectionView.frame.origin.y, 32, 32);
            okButton.frame = CGRectMake(self.editor.menuView.frame.size.width-66, 0, 66, 44);
            
        
            [okButton addTarget:self action:@selector(downButtonPressed) forControlEvents:UIControlEventTouchUpInside];
            [self.editor.view addSubview:okButton];
    }];

}
-(void)hideFontTableView {
    [okButton removeFromSuperview];
    
    [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^ {
        CGRect ftbFrame = fontCollectionView.frame;
        ftbFrame.origin.y = self.editor.view.frame.size.height;
        fontCollectionView.frame = ftbFrame;
        
        
        
    } completion:^(BOOL finished) {
    }];
    
}



#pragma mark - SHOW / HIDE COLOR PICKER VIEW ======================
-(void)showColorPickerView {
    
    NSLog(@"showColorPickerView");
    [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^ {
        CGRect ftbFrame = colorPickerView.frame;
        ftbFrame.origin.y = _menuScroll.top - colorPickerView.frame.size.height;
        colorPickerView.frame = ftbFrame;
        
        
    } completion:^(BOOL finished) {
        
        okButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [okButton setImage:[UIImage imageNamed:@"ttOkButt"] forState:UIControlStateNormal];
        okButton.frame = CGRectMake(colorPickerView.frame.size.width -32, colorPickerView.frame.origin.y, 32, 32);
        [okButton addTarget:self action:@selector(downButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [self.editor.view addSubview:okButton];
    }];
    
}
-(void)hideColorPickerView {
    [okButton removeFromSuperview];
    
    [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^ {
        CGRect ftbFrame = colorPickerView.frame;
        ftbFrame.origin.y = self.editor.view.frame.size.height;
        colorPickerView.frame = ftbFrame;
        
        
    } completion:^(BOOL finished) {
    }];
    
}

#pragma mark - SHOW / HIDE COLOR PICKER VIEW ======================
-(void)show3DView {
    
    NSLog(@"show3DView");
    [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^ {
            
            CGRect ftbFrame = _perspectiveOptionView.frame;
            ftbFrame.origin.y = _menuScroll.top - _perspectiveOptionView.frame.size.height;
            _perspectiveOptionView.frame = ftbFrame;

            
      

        
        
    } completion:^(BOOL finished) {
        
        okButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [okButton setImage:[UIImage imageNamed:@"ttOkButt"] forState:UIControlStateNormal];
        okButton.frame = CGRectMake(_perspectiveOptionView.frame.size.width -32, _perspectiveOptionView.frame.origin.y, 32, 32);
        [okButton addTarget:self action:@selector(downButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [self.editor.view addSubview:okButton];
    }];
    
}
-(void)hide3DView {
    [okButton removeFromSuperview];
    
    [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^ {
        
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
        {
            CGRect ftbFrame = _perspectiveOptionView.frame;
            ftbFrame.origin.y = self.editor.view.frame.size.height+100;
            _perspectiveOptionView.frame = ftbFrame;

        }else{
            
            CGRect ftbFrame = _perspectiveOptionView.frame;
            ftbFrame.origin.y = self.editor.view.frame.size.height;
            _perspectiveOptionView.frame = ftbFrame;

        }
        
        
    } completion:^(BOOL finished) {
    }];
    
}

#pragma mark - SHOW / HIDE SLIDER VIEW ======================
-(void)showSliderView {
      NSLog(@"self.editor.menuView %@",_menuScroll);
    [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^ {
        
         CGRect ftbFrame = _sliderView.frame;
        ftbFrame.origin.y = _menuScroll.frame.origin.y-_menuScroll.frame.size.height;
        _sliderView.frame = ftbFrame;
        
        
    } completion:^(BOOL finished) {
    }];
    
    
  
    
}
-(void)hideSliderView {
    
    
    NSLog(@"hideSliderView");
    [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^ {
        CGRect ftbFrame = _sliderView.frame;
        ftbFrame.origin.y = self.editor.view.frame.size.height;
        _sliderView.frame = ftbFrame;
        
        
    } completion:^(BOOL finished) {
    }];
    
}

-(void) downButtonPressed
{
    [self hideFontTableView];
    [self hideColorPickerView];
    [self showSliderView];
    [self hide3DView];
    
}

#pragma mark - CLEANUP ======================
- (void)cleanup  {
    
  //  [self.editor resetZoomScaleWithAnimated:true];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [_settingView endEditing:true];
    
    // Remove all the UIViews
    [_settingView removeFromSuperview];
    [_workingView removeFromSuperview];
    [_opacitySlider removeFromSuperview];
    [_dropShadowSlider removeFromSuperview];
    [_sliderView removeFromSuperview];
    [fontTableView removeFromSuperview];
    [fontCollectionView removeFromSuperview];
    
    [colorPickerView removeFromSuperview];
    [okButton removeFromSuperview];
    
    [_perspectiveOptionView removeFromSuperview];
    
    
    [_indicatorView removeFromSuperview];

    [UIView animateWithDuration:kImageToolAnimationDuration
        animations:^{
            _menuScroll.transform = CGAffineTransformMakeTranslation(0, self.editor.view.height-_menuScroll.top);
    }
    completion:^(BOOL finished) {
        [_menuScroll removeFromSuperview];
    }];
}

- (void)executeWithCompletionBlock:(void (^)(UIImage *, NSError *, NSDictionary *))completionBlock
{
    [_TextView setActiveTextView:nil];
    
    // An indicatorView appears and start animating
  
    dispatch_async(dispatch_get_main_queue(), ^{
        _indicatorView = [ImageEditorTheme indicatorView];
        _indicatorView.center = self.editor.view.center;
        [self.editor.view addSubview:_indicatorView];
        [_indicatorView startAnimating];
    });

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UIImage *image = [self buildImage:_originalImage];
      
        dispatch_async(dispatch_get_main_queue(), ^{
            [self cleanup];
            
            completionBlock(
            [self buildImage:image], nil, nil);
        });
    });
}

-(void)refreshViewAfterInApp
{
    NSLog(@"refreshViewAfterInApp");
    [fontCollectionView reloadData];
    if(isAdRemoved)
    {
        _menuScroll.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height-55 , [UIScreen mainScreen].bounds.size.width , 55);
        [self showFontTableView];
    }
    
}


#pragma mark- TEXTVIEW EDITOR OVER THE PHOTO - METHODS ============

- (UIImage*)buildImage:(UIImage*)image {
    
//    UIGraphicsBeginImageContext(image.size);
//  //  UIGraphicsBeginImageContextWithOptions(image.size, true ,[UIScreen mainScreen].scale);
//   
//    [image drawAtPoint:CGPointZero];
//    CGFloat scale = image.size.width / _workingView.width;
//    CGContextScaleCTM(UIGraphicsGetCurrentContext(), scale, scale);
//    [_workingView.layer renderInContext:UIGraphicsGetCurrentContext()];
//   
//    UIImage *tmp = UIGraphicsGetImageFromCurrentImageContext();
//    
//    UIGraphicsEndImageContext();
    
    
    //##########
    UIGraphicsBeginImageContextWithOptions(image.size, true, 0.0f);
    [image drawAtPoint:CGPointZero];
    CGFloat scale = image.size.width / _workingView.width;
    CGContextScaleCTM(UIGraphicsGetCurrentContext(), scale, scale);

    [_workingView drawViewHierarchyInRect:_workingView.bounds afterScreenUpdates:NO];
    UIImage *snapshotImageFromMyView = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return snapshotImageFromMyView;
}

- (void)setMenuBtnEnabled:(BOOL)enabled
{
    _textBtn.userInteractionEnabled =
    _colorBtn.userInteractionEnabled =
    _fontBtn.userInteractionEnabled =
    _3DBtn.userInteractionEnabled =
    

    
    _alignLeftBtn.userInteractionEnabled =
    _alignCenterBtn.userInteractionEnabled =
    _alignRightBtn.userInteractionEnabled = enabled;
}


- (void)setSelectedTextView:(_TextView *)selectedTextView
{
    if(selectedTextView != _selectedTextView){
        _selectedTextView = selectedTextView;
    }
    
    [self setMenuBtnEnabled:(_selectedTextView!=nil)];
    
    if(_selectedTextView==nil){
        [self hideSettingView];
        
        _colorBtn.iconView.backgroundColor = [UIColor clearColor];
        //_settingView.selectedFillColor;
        _alignLeftBtn.selected = _alignCenterBtn.selected = _alignRightBtn.selected = NO;
        
        _settingView.tintColor=[UIColor whiteColor];
    } else {
        /*
        _colorBtn.iconView.backgroundColor = selectedTextView.fillColor;
        _colorBtn.iconView.layer.borderColor = selectedTextView.borderColor.CGColor;
        _colorBtn.iconView.layer.borderWidth = MAX(2, 10*selectedTextView.borderWidth);
        */
        
        _settingView.selectedText = selectedTextView.text;
        _settingView.selectedFillColor = selectedTextView.fillColor;
        _settingView.selectedBorderColor = selectedTextView.borderColor;
        _settingView.selectedBorderWidth = selectedTextView.borderWidth;
        _settingView.selectedFont = selectedTextView.font;
        
        [self setTextAlignment:selectedTextView.textAlignment];
    }
}

- (void)activeTextViewDidChange:(NSNotification*)notification
{
    self.selectedTextView = notification.object;
    _dropShadowSlider.value=self.selectedTextView.layer.shadowOpacity;
    _opacitySlider.value=self.selectedTextView.alpha;
    
}

- (void)activeTextViewDidTap:(NSNotification*)notification
{
    [self beginTextEditing];
}


#pragma mark - ADD A NEW TEXTVIEW (_TextView) ============
- (void)addNewText  {
    
    _TextView *view = [_TextView new];
    view.fillColor = [UIColor whiteColor];
    //_settingView.selectedFillColor;
    view.borderColor = _settingView.selectedBorderColor;
    view.borderWidth = _settingView.selectedBorderWidth;

    view.font = [UIFont systemFontOfSize:12.0f];
    NSLog(@"selectedFont %@",view.font);
    
    
  //  view.layer.shadowOpacity=_dropShadowSlider.value;
  //  view.alpha=_opacitySlider.value;
    
    _dropShadowSlider.value=1.0;
    _opacitySlider.value=1.0;
    
    
    CGFloat ratio = MIN( (0.6 * _workingView.width) / view.width, (0.2 * _workingView.height) / view.height);
    [view setScale:ratio];
    view.center = CGPointMake(_workingView.width/2, view.height/2 + 10);
    
    [_workingView addSubview:view];
    
    // Activate a new CLTextView ========
    [_TextView setActiveTextView:view];
    
    [self beginTextEditing];
}

- (void)beginTextEditing {
    
    [_settingView becomeFirstResponder];
    [self hideFontTableView];
    [self hideColorPickerView];
}


- (void)hideSettingView
{
    [_settingView endEditing: true];
    _settingView.hidden = true;
}


- (void)setTextAlignment:(NSTextAlignment)alignment
{
    self.selectedTextView.textAlignment = alignment;
    
    _alignLeftBtn.selected =
    _alignCenterBtn.selected =
    _alignRightBtn.selected =
    false;
    
    switch (alignment) {
        case NSTextAlignmentLeft:
            _alignLeftBtn.selected = true;
            break;
        case NSTextAlignmentCenter:
            _alignCenterBtn.selected = true;
            break;
        case NSTextAlignmentRight:
            _alignRightBtn.selected = true;
            break;
        default:
            break;
    }
}


-(void)hideKeyboard {
    [_settingView resignFirstResponder];
    [self hideFontTableView];
    [self showSliderView];
    [self hide3DView];
    
    [self hideColorPickerView];
}



#pragma mark- Setting view delegate ===================

- (void)textSettingView:(TextSettingView *)settingView didChangeText:(NSString *)text
{
   
//    self.selectedTextView.text = text;
//    [self.selectedTextView sizeToFitWithMaxWidth:
//     0.8 * _workingView.width lineHeight: 0.2 * _workingView.height];
//    NSLog(@"didChangeText");
    
    if(text.length==0){
        
         self.selectedTextView.text = @"TYPE TO EDIT";
         [self.selectedTextView sizeToFitWithMaxWidth: 0.6 * _workingView.width lineHeight: 0.2 * _workingView.height];
    
    }
    else
    {
        self.selectedTextView.text = text;
        [self.selectedTextView sizeToFitWithMaxWidth:
         self.selectedTextView.width lineHeight: self.selectedTextView.height];
        
    }
}


- (void)textSettingView:(TextSettingView*)settingView didChangeFillColor:(UIColor*)fillColor
{
   // _colorBtn.iconView.backgroundColor = fillColor;
    _colorBtn.iconView.backgroundColor = [UIColor clearColor];
    self.selectedTextView.fillColor = fillColor;
}

- (void)textSettingView:(TextSettingView*)settingView didChangeBorderColor:(UIColor*)borderColor
{
    _colorBtn.iconView.layer.borderColor = borderColor.CGColor;
    self.selectedTextView.borderColor = borderColor;
}

- (void)textSettingView:(TextSettingView*)settingView didChangeBorderWidth:(CGFloat)borderWidth
{
    _colorBtn.iconView.layer.borderWidth = MAX(2, 10*borderWidth);
    self.selectedTextView.borderWidth = borderWidth;
}


- (void)textSettingView:(TextSettingView *)settingView didChangeFont:(UIFont *)font
{
    self.selectedTextView.font = font;
 //   _txtView.font = font;
//    [self.selectedTextView sizeToFitWithMaxWidth:
//     0.8 *_workingView.width lineHeight:0.2*_workingView.height];
//    
    [self.selectedTextView sizeToFitWithMaxWidth:
      self.selectedTextView.width lineHeight: self.selectedTextView.height];
   // NSLog(@"fontName:%@", font);
}

#pragma mark - OPACITY SLIDER ===================
- (void)sliderDidChange:(UISlider*)sender {
    
    NSLog(@"self.selectedTextView %@",self.selectedTextView);
    self.selectedTextView.alpha=sender.value;

}

-(void)sliderDidChangeShadow: (UISlider*) sender
{
    NSLog(@"self.selectedTextView %@",self.selectedTextView);
    self.selectedTextView.layer.shadowOpacity=sender.value;

}

#pragma mark- SLIDER CONTAINER AND SLIDER SETTINGS ===========

- (UISlider*)sliderWithValue:(CGFloat)value minimumValue:(CGFloat)min maximumValue:(CGFloat)max withTitle:(NSString*)title action:(SEL)action
{
    UISlider *slider = [[UISlider alloc] initWithFrame:CGRectMake(0, 0, (self.editor.view.width-40)/2, 35)];
    
    UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, (self.editor.view.width-40)/2, slider.height)];
    container.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.0];
    container.layer.cornerRadius = slider.height/2;
    
    slider.continuous = YES;
    [slider addTarget:self action:action forControlEvents:UIControlEventValueChanged];
    
    slider.maximumValue = max;
    slider.minimumValue = min;
    slider.value = value;
    
    /*==========================================
     Toolbar Labels Settings
     ===========================================*/
    UILabel *sliderLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, container.bottom,  (self.editor.view.width-40)/2, 12)];
    sliderLabel.backgroundColor = [UIColor clearColor];
    sliderLabel.textColor = [UIColor darkGrayColor];
    sliderLabel.font = REGULAR_FONT;
    if([title isEqualToString:NSLocalizedString(@"OPACITY", @"")])
    {
        sliderLabel.center = CGPointMake(self.editor.view.width/4, sliderLabel.center.y);
        
    }
    else
    {
        sliderLabel.center = CGPointMake((self.editor.view.width/4)*3, sliderLabel.center.y);
        
    }
    sliderLabel.text=title;
    sliderLabel.textAlignment = NSTextAlignmentCenter;
    [_sliderView addSubview:sliderLabel];
    
    
    [container addSubview:slider];
    [_sliderView addSubview:container];
    
    
    return slider;
}



@end






#pragma mark- _TextView =============
@implementation _TextView  {
   
    TextViewEdit * _txtView;
    
    UIButton *_deleteButton;
    CircleView *_circleView;
    
    CGFloat _scale;
    CGFloat _arg;
    
    CGPoint _initialPoint;
    CGFloat _initialArg;
    CGFloat _initialScale;
    
    TextTool *txtTool;
    
    //    CGPoint previousPoint;
    //    CGPoint lastPoint;
    //    CGMutablePathRef path;
    //    CGContextRef context;
    //    UIButton *btnClose;
    //    UIButton *btnErase;
    //    BOOL IsErase;
}

+ (void)setActiveTextView:(_TextView *)view
{
    static _TextView *activeView = nil;
    if(view != activeView){
        [activeView setActive:NO];
        activeView = view;
        [activeView setActive:true];
        
       
        
        [activeView.superview bringSubviewToFront:activeView];
        
        NSNotification *n = [NSNotification notificationWithName:TextViewActiveViewDidChangeNotification object:view userInfo:nil];
        [[NSNotificationCenter defaultCenter] performSelectorOnMainThread:@selector(postNotification:) withObject:n waitUntilDone:NO];
    }
}

- (id)init {
    
    self = [super initWithFrame:CGRectMake(0, 0, 132, 132)];
    if(self){
        
        // path = CGPathCreateMutable();
        
        self.backgroundColor=[UIColor clearColor];
        
        // Customize the Text Field ============
        _txtView = [[TextViewEdit alloc] init];
        _txtView.backgroundColor = [UIColor clearColor];
        _txtView.layer.borderColor = [[UIColor whiteColor] CGColor];
        _txtView.layer.borderWidth = 2;
        _txtView.layer.cornerRadius = 20;
        //_txtView.font = [UIFont fontWithName:@"AvenirNext-DemiBold" size:100];
        _txtView.contentScaleFactor = 15/200.0;
        _txtView.textAlignment = NSTextAlignmentLeft;
        _txtView.tintColor = [UIColor blackColor];
        _txtView.allowsEditingTextAttributes = false;
        // _txtView.adjustsFontSizeToFitWidth = true;
        _txtView.returnKeyType = UIReturnKeyDefault;
        //_txtView.editable=NO;
        _txtView.selectable=NO;
        
        
        _txtView.text=@"TYPE TO EDIT";
        //_txtView.textColor=[UIColor colorWithWhite:1.0 alpha:0.8];
        _txtView.layer.masksToBounds = NO;
        _txtView.clipsToBounds=NO;
        
        _txtView.layer.shadowColor = [UIColor blackColor].CGColor;
        _txtView.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
        _txtView.layer.shadowOpacity = 1.0f;
        _txtView.layer.shadowRadius = 10.0f;
        
        // Text Field Placeholder =========
        //   UIColor *placeholderColor = [UIColor colorWithWhite:1.0 alpha:0.8];
        //    _txtView.attributedPlaceholder = [[NSAttributedString alloc]
        //     initWithString:@"TAP TO EDIT" attributes:@{ NSForegroundColorAttributeName: placeholderColor }];
        
        _txtView.delegate = self;
        //  self.text = @"TAP TO EDIT";
        
        
        CGSize size = [_txtView sizeThatFits:CGSizeMake(FLT_MAX, FLT_MAX)];
        _txtView.frame = CGRectMake(16, 16, size.width, size.height);
        self.frame = CGRectMake(0, 0, size.width + 32, size.height + 32);
        [self addSubview:_txtView];
        
        
        
        // Delete TextView Button =================
        _deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_deleteButton setImage:[UIImage imageNamed:@"ttDeleteButt"] forState:UIControlStateNormal];
        _deleteButton.frame = CGRectMake(0, 0, 36, 36);
        _deleteButton.center = _txtView.frame.origin;
        [_deleteButton addTarget:self action:@selector(pushedDeleteBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_deleteButton];
        
        
        // CircleView (Handler) ======================
        _circleView = [[CircleView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
        _circleView.center = CGPointMake(_txtView.width + _txtView.left, _txtView.height + _txtView.top);
        _circleView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin;
        _circleView.radius = 0.7;
        _circleView.color = [UIColor whiteColor];
        _circleView.borderColor = [UIColor purpleColor];
        _circleView.borderWidth = 2;
        [self addSubview:_circleView];
        
        
        _arg = 0;
        [self setScale:1];
        
        [self initGestures];
        
        
    }
    return self;
}



/*
#pragma mark - TEXT FIELD DELEGATES =========================

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [_txtView resignFirstResponder];
    return true;
}


-(BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    _txtView.allowsEditingTextAttributes = true;
    _txtView.adjustsFontSizeToFitWidth = true;
    _txtView.returnKeyType = UIReturnKeyDone;
    [_txtView becomeFirstResponder];
    return true;
}


-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    _txtView.allowsEditingTextAttributes = true;
    _txtView.adjustsFontSizeToFitWidth = true;
    _txtView.returnKeyType = UIReturnKeyDone;
    self.text = @"";
    [[self class] setActiveTextView:self];

    return  true;
}



-(void)textFieldDidBeginEditing:(UITextField *)textField {
    _txtView.allowsEditingTextAttributes = true;
    _txtView.adjustsFontSizeToFitWidth = true;
    _txtView.returnKeyType = UIReturnKeyDone;
    _txtView.keyboardAppearance = UIKeyboardAppearanceDark;
   // NSLog(@"BEGIN-Edit");
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
   // NSLog(@"END-Edit");
    [_txtView resignFirstResponder];
}

*/

#pragma mark - INIT GESTURE RECOGNIZERS ===================
- (void)initGestures
{
    _txtView.userInteractionEnabled = true;
    
    [_txtView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewDidTap:)]];
    [_txtView addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(viewDidPan:)]];
   
    [_circleView addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(circleViewDidPan:)]];
    
}


#pragma mark- TextView PROPERTIES ==================

- (void)setActive:(BOOL)active
{
    _deleteButton.hidden = !active;
    _circleView.hidden = !active;
    _txtView.layer.borderWidth = (active) ? 1/_scale : 0;
}

- (BOOL)active
{
    return !_deleteButton.hidden;
}


- (void)sizeToFitWithMaxWidth:(CGFloat)width lineHeight:(CGFloat)lineHeight
{
    self.transform = CGAffineTransformIdentity;
    _txtView.transform = CGAffineTransformIdentity;
    
   CGSize size = [_txtView sizeThatFits:CGSizeMake(width , lineHeight)]; _txtView.frame = CGRectMake(16, 16, size.width, size.height);
    
    CGFloat viewW = (_txtView.width +32);
    CGFloat viewH = _txtView.font.lineHeight;
    
    CGFloat ratio = MIN(width / viewW,  lineHeight / viewH);

    [self setScale:ratio];
    NSLog(@"ratio: %f", ratio);


}



- (void)setScale:(CGFloat)scale  {
    
    _scale = scale;
    
    self.transform = CGAffineTransformIdentity;
    //_txtView.transform = CGAffineTransformMakeScale(_scale, _scale);
    
    CGRect rct = self.frame;
    rct.origin.x += (rct.size.width - (_txtView.width + 32)) / 2;
    rct.origin.y += (rct.size.height - (_txtView.height + 32)) / 2;
    rct.size.width  = _txtView.width + 32;
    rct.size.height = _txtView.height + 32;
    self.frame = rct;
    
    _txtView.center = CGPointMake(rct.size.width/2, rct.size.height/2);
    
    self.transform = CGAffineTransformMakeRotation(_arg);
    
    _txtView.layer.borderWidth = 1/_scale;
    _txtView.layer.cornerRadius = 3/_scale;
    
   // NSLog(@"scale: %f", scale);

}

- (void)updateTextFont:(UITextView *)textView
{
    
    // Only run if has text, otherwise it will make infinity loop
    if (textView.text.length == 0 || CGSizeEqualToSize(textView.bounds.size, CGSizeZero)) return;
    
    NSLog(@"updateTextFont %f ",textView.bounds.size.width);
    
    /*
     - Update textView font size
     If expectHeight > textViewHeight => descrease font size n point until it reach textViewHeight
     If expectHeight < textViewHeight => inscrease font size n point until it reach textViewHeight
     */
    CGSize textViewSize = textView.frame.size;
    CGFloat fixedWidth = textViewSize.width;
    CGSize expectSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    
//    if(!textView.font)
//    {
//        textView.font=self.font;
//    }
//    
    UIFont *expectFont = textView.font;
    NSLog(@"updateTextFont %@ ",expectFont);
    
    if (expectSize.height > textViewSize.height) {
        while (  [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)].height > textViewSize.height) {
            expectFont = [textView.font fontWithSize:(textView.font.pointSize-1)];
            textView.font = expectFont;
        }
    } else {
        while ([textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)].height < textViewSize.height) {
            expectFont = textView.font;
            textView.font = [textView.font fontWithSize:(textView.font.pointSize+1)];
        }
        textView.font = expectFont;
    }
    
    NSLog(@"updateTextFont expectFont %@",expectFont);
    
    
}
- (void)setFillColor:(UIColor *)fillColor
{
    _txtView.textColor = fillColor;
}

- (UIColor*)fillColor
{
    return _txtView.textColor;
}


- (void)setBorderColor:(UIColor *)borderColor
{
    _txtView.outlineColor = borderColor;
}

- (UIColor*)borderColor
{
    return _txtView.outlineColor;
}

- (void)setBorderWidth:(CGFloat)borderWidth
{
    _txtView.outlineWidth = borderWidth;
}

- (CGFloat)borderWidth
{
    return _txtView.outlineWidth;
}



- (void)setFont:(UIFont *)font
{
    _txtView.font = font;
}

- (UIFont *)font
{
    return _txtView.font;
}

- (void)setTextAlignment:(NSTextAlignment)textAlignment
{
    _txtView.textAlignment = textAlignment;
}

- (NSTextAlignment)textAlignment
{
    return _txtView.textAlignment;
}

- (void)setText:(NSString *)text
{
    if(![text isEqualToString:_text]){
        _text = text;
        _txtView.text = (_text.length > 0) ? _text : @"";
        //_txtView.placeholder = @"";
        //NSLocalizedStringWithDefaultValue(@"CLTextTool_EmptyText", nil, [ImageEditorTheme bundle], @"Text", @"");
        }
    
}



#pragma mark- GESTURE EVENTS ================
- (void)pushedDeleteBtn:(id)sender
{
    _TextView *nextTarget = nil;
    
    const NSInteger index = [self.superview.subviews indexOfObject:self];
    
    for (NSInteger i = index+1; i < self.superview.subviews.count; ++i){
        UIView *view = [self.superview.subviews objectAtIndex:i];
        if([view isKindOfClass:[_TextView class]]){
            nextTarget = (_TextView*)view;
            break;
        }
    }
    
    if(nextTarget == nil){
        for(NSInteger i=index-1; i>=0; --i){
            UIView *view = [self.superview.subviews objectAtIndex:i];
            if([view isKindOfClass:[_TextView class]]){
                nextTarget = (_TextView*)view;
                break;
            }
        }
    }
    
    [[self class] setActiveTextView:nextTarget];
    [self removeFromSuperview];
}


- (void)viewDidTap:(UITapGestureRecognizer*)sender
{
    if(self.active){
        NSNotification *n = [NSNotification notificationWithName:TextViewActiveViewDidTapNotification object:self userInfo:nil];
        [[NSNotificationCenter defaultCenter] performSelectorOnMainThread:@selector(postNotification:) withObject:n waitUntilDone:NO];
    }
    
    [[self class] setActiveTextView:self];
}

- (void)viewDidPan:(UIPanGestureRecognizer*)sender
{
    [[self class] setActiveTextView:self];
    
    CGPoint p = [sender translationInView:self.superview];
    
    if(sender.state == UIGestureRecognizerStateBegan){
        _initialPoint = self.center;
    }
    self.center = CGPointMake(_initialPoint.x + p.x, _initialPoint.y + p.y);
}

- (void)circleViewDidPan:(UIPanGestureRecognizer*)sender {
    
    CGPoint p = [sender translationInView:self.superview];
    
    static CGFloat tmpR = 1;
    static CGFloat tmpA = 0;
    
    if(sender.state == UIGestureRecognizerStateBegan){
        _initialPoint = [self.superview convertPoint:_circleView.center fromView:_circleView.superview];
        
        CGPoint p = CGPointMake(_initialPoint.x - self.center.x, _initialPoint.y - self.center.y);
        tmpR = sqrt(p.x*p.x + p.y*p.y);
        tmpA = atan2(p.y, p.x);
        
        _initialArg = _arg;
        _initialScale = _scale;
    }
    
    p = CGPointMake(_initialPoint.x + p.x - self.center.x, _initialPoint.y + p.y - self.center.y);
    CGFloat R = sqrt(p.x*p.x + p.y*p.y);
    CGFloat arg = atan2(p.y, p.x);
    
    _arg   = _initialArg + arg - tmpA;
    [self setScale:MAX(_initialScale * R / tmpR, 15/200.0)];
    
    _txtView.transform = CGAffineTransformMakeScale(MAX(_initialScale * R / tmpR, 15/200.0), MAX(_initialScale * R / tmpR, 15/200.0));

    
    if(sender.state==UIGestureRecognizerStateEnded )
    {
        NSLog(@"UIGestureRecognizerStateEnded");
        [self updateTextFont:_txtView];
        [self sizeToFitWithMaxWidth:_txtView.width lineHeight:_txtView.height];
        
    }
}

-(void) tiltDegrees:(CGFloat)degrees atXaxis:(BOOL)isXaxis {
    CATransform3D aTransform = CATransform3DIdentity;
    CGFloat zDistance = 100; // affects the sharpness of the transform
    aTransform.m34 = 1.0 / -zDistance;
    if(isXaxis)
    {
        aTransform = CATransform3DRotate(aTransform, degrees * M_PI / 180.0f, 0.0f, 1.0f, 0.0f);
        
    }
    else
    {
        aTransform = CATransform3DRotate(aTransform, degrees * M_PI / 180.0f, 1.0f, 0.0f, 0.0f);
        
    }
    self.layer.transform = aTransform;
}


@end



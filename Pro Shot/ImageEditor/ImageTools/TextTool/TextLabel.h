

#import <UIKit/UIKit.h>

@interface TextLabel : UILabel
//UILabel

@property (nonatomic, strong) UIColor *outlineColor;
@property (nonatomic, assign) CGFloat outlineWidth;

@end



#import <UIKit/UIKit.h>

@interface TextViewEdit : UITextView
<
UITextFieldDelegate,
UITextViewDelegate
>


@property (nonatomic, strong) UIColor *outlineColor;
@property (nonatomic, assign) CGFloat outlineWidth;

@end

//
//  FontCollectionViewCell.h
//  Pro Shot
//
//  Created by Tanvir Palash on 5/3/17.
//  Copyright © 2017 FV iMAGINATION. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FontCollectionViewCell : UICollectionViewCell

@property (retain, nonatomic) UILabel* label;
@property(retain,nonatomic) UIImageView *imageView;
@property(retain,nonatomic) UIImageView *iapDot;



@end

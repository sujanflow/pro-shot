/*============================
 
 Pro Shot
 
 iOS 7/8 iPhone Photo Editor App template
 created by FV iMAGINATION - 2014
 http://www.fvimagination.com
 
 ==============================*/


#import "ImageToolBase.h"
#import "Appirater.h"

NSArray *filesList;
NSString *filePath, *texturesPath;

UIView *_workingView;
UIImage *_originalImage;

CGFloat width, height;



@interface TexturesTool : ImageToolBase <AppiraterDelegate>

@end

/*============================
 
 Pro Shot
 
 iOS 7/8 iPhone Photo Editor App template
 created by FV iMAGINATION - 2014
 http://www.fvimagination.com
 
 ==============================*/


#import "StickerTool.h"
#import "CircleView.h"
#import "IAPController.h"
#import "Configs.h"
#import "Appirater.h"


static NSString* const kStickerToolStickerPathKey = @"stickerPath";

@interface _StickerView : UIView
+ (void)setActiveStickerView:(_StickerView *)view;
- (UIImageView*)getImageView;
- (id)initWithImage:(UIImage *)image;
- (void)setScale:(CGFloat)scale;
@end



@implementation StickerTool
{
    UIImage *_originalImage;
    UIView *_workingView;
    
    UIScrollView *_menuScroll;
    UIScrollView *_submenuScroll;
    NSString *_StickerType;
    
    
    UIView *_sliderView;
    UISlider *_opacitySlider;
    UISlider *_dropShadowSlider;
    UILabel *_opacityLabel;
    UILabel *_shadowLabel;
    
    
    // For IAP =========
    UIImageView *iapDot;
  //  NSTimer *iapTimer;
    int tagINT,
    stickersTAG;
    
}

+ (NSArray*)subtools {
    return nil;
}

+ (NSString*)defaultTitle {
    return NSLocalizedString(@"StickerTool", @"");
}

+ (BOOL)isAvailable {
    return true;
}




#pragma mark- STICKER PATHS ============
// Default Stickers Path ===============
+ (NSString*)defaultStickerPath
{
   // return [[[ImageEditorTheme bundle] bundlePath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", NSStringFromClass(self)]];
    return [[[ImageEditorTheme bundle] bundlePath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/stickers", NSStringFromClass(self)]];
    
}

+ (NSString*)defaultStickerPathThumb
{
   //return [[[ImageEditorTheme bundle] bundlePath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/stickers/thumb", NSStringFromClass(self)]];
    return [[[ImageEditorTheme bundle] bundlePath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/stickers/", NSStringFromClass(self)]];
}
+ (NSDictionary*)optionalInfo
{
    return @{kStickerToolStickerPathKey:[self defaultStickerPath]};
}
//========================================





#pragma mark- INITIALIZATION ==========================

- (void)setup {
    
    _originalImage = self.editor.imageView.image;
    
    // Fire IAP timer
  //  iapTimer = [NSTimer scheduledTimerWithTimeInterval:0.2  target:self selector:@selector(removeIapDots:)  userInfo:nil repeats:true];
    
    
    _workingView = [[UIView alloc] initWithFrame:[self.editor.view convertRect:self.editor.imageView.frame fromView:self.editor.imageView.superview]];
    _workingView.clipsToBounds = true;
    [self.editor.view addSubview:_workingView];
    
    _menuScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0,self.editor.menuView.frame.origin.y-self.editor.menuView.frame.size.height-self.editor.menuView.frame.size.height, self.editor.menuView.frame.size.width,self.editor.menuView.frame.size.height)];
    _menuScroll.backgroundColor = self.editor.menuView.backgroundColor;
    _menuScroll.showsHorizontalScrollIndicator = true;
    [self.editor.view addSubview:_menuScroll];
    
   
    
    _submenuScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0,self.editor.menuView.frame.origin.y-self.editor.menuView.frame.size.height, self.editor.menuView.frame.size.width,self.editor.menuView.frame.size.height)];
    _submenuScroll.backgroundColor = self.editor.menuView.backgroundColor;
    _submenuScroll.showsHorizontalScrollIndicator = NO;
    [self.editor.view addSubview:_submenuScroll];
    
    

    
    
//    _sliderView = [[UIView alloc] initWithFrame:CGRectMake(0,self.editor.menuView.frame.origin.y-self.editor.menuView.frame.size.height, self.editor.menuView.frame.size.width,self.editor.menuView.frame.size.height)];
    _sliderView = [[UIView alloc] initWithFrame:self.editor.menuView.frame];
    
    _sliderView.backgroundColor = [MENU_COLOR colorWithAlphaComponent:1.0];
    [self.editor.view addSubview:_sliderView];
    
    
    
    [self setStickerMenu];
    [self setupSlider];
    
    _menuScroll.transform = CGAffineTransformMakeTranslation(0, _menuScroll.height);
    
    
    _submenuScroll.transform = CGAffineTransformMakeTranslation(0, self.editor.view.height-_submenuScroll.top);
    [UIView animateWithDuration:kImageToolAnimationDuration
                     animations:^{
                         _submenuScroll.transform = CGAffineTransformIdentity;
                     }];
    
    _submenuScroll.hidden = YES;
    
}

- (void)cleanup {
    //[self.editor resetZoomScaleWithAnimated:true];
    
    
//    [iapTimer invalidate];
    [_workingView removeFromSuperview];
    [_opacitySlider removeFromSuperview];
    [_dropShadowSlider removeFromSuperview];
    [_sliderView removeFromSuperview];
    
    [UIView animateWithDuration:kImageToolAnimationDuration animations:^{
        _menuScroll.transform = CGAffineTransformMakeTranslation(0, self.editor.view.height-_menuScroll.top);
        
    } completion:^(BOOL finished) {
        [_menuScroll removeFromSuperview];
    }];
    
    [UIView animateWithDuration:kImageToolAnimationDuration
                     animations:^{
                         _submenuScroll.transform = CGAffineTransformMakeTranslation(0, self.editor.view.height-_submenuScroll.top);
                     }
                     completion:^(BOOL finished) {
                         [_submenuScroll removeFromSuperview];
                     }];
}
- (void)setupSlider {
    // Set here max and min slider value
//    _opacitySlider = [self sliderWithValue:0.5 minimumValue:0 maximumValue:1 action:@selector(sliderDidChange:)];
//    _opacitySlider.superview.center = CGPointMake(self.editor.view.width/4,_workingView.bottom-30);
//    _opacitySlider.thumbTintColor = [UIColor blackColor];
//    _opacitySlider.minimumTrackTintColor = [UIColor darkGrayColor];
//    _opacitySlider.maximumTrackTintColor = LIGHT_COLOR;
// 
//    _dropShadowSlider = [self sliderWithValue:0.5 minimumValue:0 maximumValue:1 action:@selector(sliderDidChangeShadow:)];
//    _dropShadowSlider.superview.center = CGPointMake((self.editor.view.width/4)*3,_workingView.bottom-30);
//    _dropShadowSlider.thumbTintColor = [UIColor blackColor];
//    _dropShadowSlider.minimumTrackTintColor = [UIColor darkGrayColor];
//    _dropShadowSlider.maximumTrackTintColor = LIGHT_COLOR;
    
    _opacitySlider = [self sliderWithValue:0.5 minimumValue:0 maximumValue:1 withTitle:NSLocalizedString(@"OPACITY", @"") action:@selector(sliderDidChange:)];
    _opacitySlider.superview.center = CGPointMake(self.editor.view.width/4,_opacitySlider.frame.size.height/2);
    _opacitySlider.thumbTintColor = [UIColor whiteColor];
    _opacitySlider.minimumTrackTintColor = [UIColor darkGrayColor];
    _opacitySlider.maximumTrackTintColor = LIGHT_COLOR;
    
    _dropShadowSlider = [self sliderWithValue:0.5 minimumValue:0 maximumValue:1 withTitle:NSLocalizedString(@"SHADOW", @"") action:@selector(sliderDidChangeShadow:)];
    _dropShadowSlider.superview.center = CGPointMake((self.editor.view.width/4)*3,_dropShadowSlider.frame.size.height/2);
    _dropShadowSlider.thumbTintColor = [UIColor whiteColor];
    _dropShadowSlider.minimumTrackTintColor = [UIColor darkGrayColor];
    _dropShadowSlider.maximumTrackTintColor = LIGHT_COLOR;
    
   
    
}

#pragma mark - OPACITY SLIDER ===================
- (void)sliderDidChange:(UISlider*)sender {
   
    _StickerView *view=(_StickerView*)[[_workingView subviews] lastObject];
    [view getImageView].alpha=sender.value;
    
}

-(void)sliderDidChangeShadow: (UISlider*) sender
{
    _StickerView *view=(_StickerView*)[[_workingView subviews] lastObject];
    [view getImageView].layer.shadowOpacity = sender.value;
    
}

#pragma mark- SLIDER CONTAINER AND SLIDER SETTINGS ===========

- (UISlider*)sliderWithValue:(CGFloat)value minimumValue:(CGFloat)min maximumValue:(CGFloat)max withTitle:(NSString*)title action:(SEL)action
{
    UISlider *slider = [[UISlider alloc] initWithFrame:CGRectMake(0, 0, (self.editor.view.width-40)/2, 35)];
    
    UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, (self.editor.view.width-40)/2, slider.height)];
    container.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.0];
    container.layer.cornerRadius = slider.height/2;
    
    slider.continuous = YES;
    [slider addTarget:self action:action forControlEvents:UIControlEventValueChanged];
    
    slider.maximumValue = max;
    slider.minimumValue = min;
    slider.value = value;
    
    /*==========================================
     Toolbar Labels Settings
     ===========================================*/
    UILabel *sliderLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, container.bottom,  (self.editor.view.width-40)/2, 12)];
    sliderLabel.backgroundColor = [UIColor clearColor];
    sliderLabel.textColor = [UIColor darkGrayColor];
    sliderLabel.font = REGULAR_FONT;
    if([title isEqualToString:NSLocalizedString(@"OPACITY", @"")])
    {
        sliderLabel.center = CGPointMake(self.editor.view.width/4, sliderLabel.center.y);
        
    }
    else
    {
        sliderLabel.center = CGPointMake((self.editor.view.width/4)*3, sliderLabel.center.y);
        
    }
    sliderLabel.text=title;
    sliderLabel.textAlignment = NSTextAlignmentCenter;
    [_sliderView addSubview:sliderLabel];

    
    [container addSubview:slider];
    [_sliderView addSubview:container];
    
    
    return slider;
}

#pragma mark - REMOVE IAP DOTS METHOD  =================
-(void)refreshViewAfterInApp
{
    NSLog(@"refreshViewAfterInApp");
    [self removeIapDots];
    if(isAdRemoved)
    {
        self.editor.menuView.frame= CGRectMake(0, [UIScreen mainScreen].bounds.size.height-55 , [UIScreen mainScreen].bounds.size.width , 55);
        _sliderView.frame =    self.editor.menuView.frame;
        _menuScroll.frame = CGRectMake(0,self.editor.menuView.frame.origin.y-self.editor.menuView.frame.size.height-self.editor.menuView.frame.size.height, self.editor.menuView.frame.size.width,self.editor.menuView.frame.size.height);
        
        _submenuScroll.frame = CGRectMake(0,self.editor.menuView.frame.origin.y-self.editor.menuView.frame.size.height, self.editor.menuView.frame.size.width,self.editor.menuView.frame.size.height);
        
    }
    
}
// Remove iapDots icons from items that have been purchased with IAP
-(void)removeIapDots {
    if (iapForSticker || iapMade) {
        for (int i = 500+freeStickers; i <= iapDot.tag; i++) {
            [[self.editor.view viewWithTag:i] removeFromSuperview];
        }
      //  [iapTimer invalidate];
    }
    // NSLog(@"timerON!");
}


- (void)executeWithCompletionBlock:(void (^)(UIImage *, NSError *, NSDictionary *))completionBlock
{
    [_StickerView setActiveStickerView:nil];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UIImage *image = [self buildImage:_originalImage];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            completionBlock(image, nil, nil);
        });
    });
}



#pragma mark - SET STICKER MENU =================

- (void)setStickerMenu
{
    CGFloat W = 70;
    CGFloat H = _menuScroll.height;
    CGFloat x = 0;
    
    NSString *stickerPath = [[self class] defaultStickerPath];
    
    if(stickerPath==nil){ stickerPath = [[self class] defaultStickerPath]; }
    
    
    //    stickerPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingString:@"/CLMirrorTool/stickers"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSError *error = nil;
    NSArray *list = [fileManager contentsOfDirectoryAtPath:stickerPath error:&error];
    

    for(NSString *path in list){
        NSString *filePath = [NSString stringWithFormat:@"%@/%@", stickerPath, path];
        NSString *SubMenuName = [NSString stringWithFormat:@"%@", path];
        UIImage *image = [UIImage imageWithContentsOfFile:filePath];
        if(image){
            ToolbarMenuItem *item = [ImageEditorTheme menuItemWithFrame:CGRectMake(x, 0, W, H) target:self action:@selector(tappedStickerCategoryPanel:) toolInfo:nil];
            item.iconImage = [image aspectFit:CGSizeMake(50, 50)];
            // item.iconImage = [[image aspectFit:CGSizeMake(50, 50)] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            item.userInfo = @{@"filePath" : SubMenuName};
            
            [_menuScroll addSubview:item];
            x += W;
        }
    }
    _menuScroll.contentSize = CGSizeMake(MAX(x, _menuScroll.frame.size.width+1), 0);
}

- (void)setStickerSubMenu {
    
    tagINT = 0;
    stickersTAG = -1;
    
    CGFloat W = 70;
    CGFloat H = _submenuScroll.height;
    CGFloat x = 0;

    for(UIView *subview in [_submenuScroll subviews]) {
        [subview removeFromSuperview];
    }
    [_submenuScroll setContentOffset:CGPointMake(0, 0) animated:NO];
    
    NSString *stickerPath = [[self class] defaultStickerPathThumb];
    if(stickerPath==nil){ stickerPath = [[self class] defaultStickerPathThumb]; }
    
    stickerPath = [stickerPath stringByReplacingOccurrencesOfString:@"stickers" withString:_StickerType];
    
 
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    filesList = [fileManager contentsOfDirectoryAtPath:stickerPath error:&error];
    NSLog(@"stickerPath: %@", stickerPath);
    
    for (NSString *pathStr in filesList){
        filePath = [NSString stringWithFormat:@"%@/%@", stickerPath, pathStr];
        NSLog(@"FP: %@", filePath);
        
        UIImage *image = [UIImage imageWithContentsOfFile:filePath];
       
        if(image){
            ToolbarMenuItem *item = [ImageEditorTheme menuItemWithFrame:CGRectMake(x, 0, W, H) target:self action:@selector(tappedStickerPanel:) toolInfo:nil];
            item.iconImage = [image aspectFit:CGSizeMake(50, 50)];
            
           // item.iconImage = [[image aspectFit:CGSizeMake(50, 50)] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            item.userInfo = @{@"filePath" : filePath};
            item.tintColor=[UIColor darkGrayColor];
            tagINT++;
            item.tag = tagINT;
            
            // Add a little circle on the top of the PAID items that need to be unlocked with IAP
            if (!iapForSticker && !iapMade && item.tag >= freeStickers) {
                iapDot = [[UIImageView alloc]initWithFrame:CGRectMake(0, 10, 6, 6)];
                iapDot.backgroundColor = PURPLE_COLOR;
                iapDot.layer.cornerRadius = iapDot.bounds.size.width/2;
                iapDot.tag = tagINT+500;
                [item addSubview:iapDot];
                //NSLog(@"iapDot TAG: %ld", (long)iapDot.tag);
            }
            //====================================================================
            
            
            [_submenuScroll addSubview:item];
            x += W;
        }
    }
    
   //  NSLog(@"Stickers List: %@", filesList);
    
    _submenuScroll.contentSize = CGSizeMake(MAX(x, _submenuScroll.frame.size.width+1), 0);
     _submenuScroll.hidden = NO;
}


- (void)tappedStickerCategoryPanel:(UITapGestureRecognizer*)sender
{
    [UIView animateWithDuration:kImageToolAnimationDuration animations:^{
        _menuScroll.transform = CGAffineTransformIdentity;
    }];
    
    
    UIView *view = sender.view;
    NSString *SubMenuName = view.userInfo[@"filePath"];
    if(SubMenuName){
        
        NSArray* SubMenuNameArray = [SubMenuName componentsSeparatedByString: @"."];
        NSString* SubMenuName = [SubMenuNameArray objectAtIndex: 0];
        _StickerType = SubMenuName;
        NSLog(@"tappedStickerCategoryPanel %@ %@",SubMenuName,_StickerType);
    }
        [self setStickerSubMenu];
        
    }
    
   

- (void)tappedStickerPanel:(UITapGestureRecognizer*)sender {
    UIView *view = sender.view;
   // NSLog(@"tag: %ld", (long)view.tag);
    
    NSString *filePath = view.userInfo[@"filePath"];
    
    
    /*====================================================================================
     NO IAP MADE - open the IAP Controller
     =====================================================================================*/
    if (!iapForSticker && !iapMade && view.tag >= freeStickers) {
        IAPController *iapVC = [[IAPController alloc]initWithNibName:@"IAPController" bundle:nil];
        iapVC.delegate=self.editor;
        
        [self.editor presentViewController: iapVC animated:true completion:nil];
        
        
    /*========================================================================================
        IAP MADE!
    =========================================================================================*/
    } else {
        filePath = [filePath stringByReplacingOccurrencesOfString:@"/thumb" withString:@""];
        
        _StickerView *view = [[_StickerView alloc] initWithImage:[UIImage imageWithContentsOfFile:filePath]];
        CGFloat ratio = MIN( (0.5 * _workingView.width) / view.width, (0.5 * _workingView.height) / view.height);
        [view setScale:ratio];
      
        view.getImageView.layer.shadowOpacity=_dropShadowSlider.value;
        view.getImageView.alpha=_opacitySlider.value;
        
        view.center = CGPointMake(_workingView.width/2, _workingView.height/2);
        [_workingView addSubview:view];
        [_StickerView setActiveStickerView:view];
        
        stickersTAG++;
        view.tag = stickersTAG;
        
    }

}



#pragma mark- BUILD IMAGE METHOD ============
- (UIImage*)buildImage:(UIImage*)image {
    
    UIGraphicsBeginImageContextWithOptions(image.size, true, 0.0);
    [image drawAtPoint:CGPointZero];
    CGFloat scale = image.size.width / _workingView.width;
    CGContextScaleCTM(UIGraphicsGetCurrentContext(), scale, scale);
    [_workingView drawViewHierarchyInRect:_workingView.bounds afterScreenUpdates: false];
    UIImage *tmp = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return tmp;
}

@end







#pragma mark - STICKER VIEW IMPLEMENTATION ======================
@implementation _StickerView {
    
    UIImageView *_imageView;
    UIButton *_deleteButton;
    CircleView *_circleView;
    
    CGFloat _scale;
    CGFloat _arg;
    
    CGPoint _initialPoint;
    CGFloat _initialArg;
    CGFloat _initialScale;
}

+ (void)setActiveStickerView:(_StickerView*)view
{
    static _StickerView *activeView = nil;
    if(view != activeView){
        [activeView setActive:NO];
        activeView = view;
        [activeView setActive:YES];
        
        [activeView.superview bringSubviewToFront:activeView];
        
        
    }
   
}


- (id)initWithImage:(UIImage *)image
{
    self = [super initWithFrame:CGRectMake(0, 0, image.size.width+32, image.size.height+32)];
    if(self){
        
        // Alloc the ImageView that contains the selected sticker
        _imageView = [[UIImageView alloc] initWithImage:image];
        _imageView.layer.borderColor = [[UIColor whiteColor] CGColor];
        _imageView.layer.cornerRadius = 3;
        _imageView.center = self.center;
        [self addSubview:_imageView];
        
        //UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:_imageView.bounds];
        _imageView.layer.masksToBounds = NO;
        _imageView.clipsToBounds=NO;
        _imageView.layer.shadowColor = [UIColor blackColor].CGColor;
        _imageView.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
        _imageView.layer.shadowOpacity = 0.5f;
        _imageView.layer.shadowRadius = 10.0f;
        //_imageView.layer.shadowPath = shadowPath.CGPath;

        
        
        // Delete button (delete the selected sticker)
        _deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_deleteButton setImage:[UIImage imageNamed:@"st_deleteButt"] forState:UIControlStateNormal];
        _deleteButton.frame = CGRectMake(0, 0, 35, 35);
        _deleteButton.center = _imageView.frame.origin;
        [_deleteButton addTarget:self action:@selector(pushedDeleteBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_deleteButton];
        
        
        // Circle View (Resize the current sticker)
        _circleView = [[CircleView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
        _circleView.center = CGPointMake(_imageView.width + _imageView.frame.origin.x, _imageView.height + _imageView.frame.origin.y);
        _circleView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin;
        _circleView.radius = 0.7;
        _circleView.color = [UIColor whiteColor];
        _circleView.borderColor = MAIN_COLOR;
        _circleView.borderWidth = 2;
        [self addSubview:_circleView];
        
        
        _scale = 1;
        _arg = 0;
        
        [self initGestures];
    }
    return self;
}

- (void)initGestures
{
    _imageView.userInteractionEnabled = YES;
    [_imageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewDidTap:)]];
    [_imageView addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(viewDidPan:)]];
    [_circleView addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(circleViewDidPan:)]];
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    UIView* view= [super hitTest:point withEvent:event];
    if(view==self){
        return nil;
    }
    return view;
}

- (UIImageView*)getImageView
{
    return _imageView;
}

- (void)pushedDeleteBtn:(id)sender
{
    _StickerView *nextTarget = nil;
    
    const NSInteger index = [self.superview.subviews indexOfObject:self];
    
    for(NSInteger i=index+1; i<self.superview.subviews.count; ++i){
        UIView *view = [self.superview.subviews objectAtIndex:i];
        if([view isKindOfClass:[_StickerView class]]){
            nextTarget = (_StickerView*)view;
            break;
        }
    }
    
    if(nextTarget==nil){
        for(NSInteger i=index-1; i>=0; --i){
            UIView *view = [self.superview.subviews objectAtIndex:i];
            if([view isKindOfClass:[_StickerView class]]){
                nextTarget = (_StickerView*)view;
                break;
            }
        }
    }
    
    [[self class] setActiveStickerView:nextTarget];
    [self removeFromSuperview];
}

- (void)setActive:(BOOL)active
{
    _deleteButton.hidden = !active;
    _circleView.hidden = !active;
    _imageView.layer.borderWidth = (active) ? 1/_scale : 0;
}

- (void)setScale:(CGFloat)scale
{
    _scale = scale;
    
    self.transform = CGAffineTransformIdentity;
    
    _imageView.transform = CGAffineTransformMakeScale(_scale, _scale);
    
    CGRect rct = self.frame;
    rct.origin.x += (rct.size.width - (_imageView.width + 32)) / 2;
    rct.origin.y += (rct.size.height - (_imageView.height + 32)) / 2;
    rct.size.width  = _imageView.width + 32;
    rct.size.height = _imageView.height + 32;
    self.frame = rct;
    
    _imageView.center = CGPointMake(rct.size.width/2, rct.size.height/2);
    
    self.transform = CGAffineTransformMakeRotation(_arg);
    
    _imageView.layer.borderWidth = 1/_scale;
    _imageView.layer.cornerRadius = 3/_scale;
}

- (void)viewDidTap:(UITapGestureRecognizer*)sender
{
    [[self class] setActiveStickerView:self];
}

- (void)viewDidPan:(UIPanGestureRecognizer*)sender
{
    [[self class] setActiveStickerView:self];
    
    CGPoint p = [sender translationInView:self.superview];
    
    if(sender.state == UIGestureRecognizerStateBegan){
        _initialPoint = self.center;
    }
    self.center = CGPointMake(_initialPoint.x + p.x, _initialPoint.y + p.y);
}

- (void)circleViewDidPan:(UIPanGestureRecognizer*)sender
{
    CGPoint p = [sender translationInView:self.superview];
    
    static CGFloat tmpR = 1;
    static CGFloat tmpA = 0;
    if(sender.state == UIGestureRecognizerStateBegan){
        _initialPoint = [self.superview convertPoint:_circleView.center fromView:_circleView.superview];
        
        CGPoint p = CGPointMake(_initialPoint.x - self.center.x, _initialPoint.y - self.center.y);
        tmpR = sqrt(p.x*p.x + p.y*p.y);
        tmpA = atan2(p.y, p.x);
        
        _initialArg = _arg;
        _initialScale = _scale;
    }
    
    p = CGPointMake(_initialPoint.x + p.x - self.center.x, _initialPoint.y + p.y - self.center.y);
    CGFloat R = sqrt(p.x*p.x + p.y*p.y);
    CGFloat arg = atan2(p.y, p.x);
    
    _arg   = _initialArg + arg - tmpA;
    [self setScale:MAX(_initialScale * R / tmpR, 0.2)];
}

@end

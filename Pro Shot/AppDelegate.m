/*============================
 
 Pro Shot
 
 iOS 7/8 iPhone Photo Editor App template
 created by FV iMAGINATION - 2015
 http://www.fvimagination.com
 
==============================*/


#import "AppDelegate.h"
#import "HomeVC.h"
#import "Configs.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
//#import "AFHTTPRequestOperation.h"
#import "NSString+SBJSON.h"

#import "Appirater.h"



#import <GoogleMobileAds/GoogleMobileAds.h>

#import "iRate.h"


@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    
#pragma mark -  LOCAL NOTIFICATION SETUP ===============================
    if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)]){
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
    }
    UILocalNotification *locationNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if (locationNotification) {
        // Sets icon badge number to zero
        application.applicationIconBadgeNumber = 0;
    }
//  END Local Notification ==========================

    BOOL israteGiven = [[NSUserDefaults standardUserDefaults]  boolForKey:@"rated"];
    
    if (!israteGiven) {
        
      #define APPLE_ID @"1185301470"
        
        NSURL *url = [NSURL URLWithString:@"http://pysmart.com/apinayeem/check_app_status.php"];
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        [request setPostValue:APPLE_ID forKey:@"bundleid"];
        
        
        [request setDelegate:self];
        [request startAsynchronous];
        
//            [Appirater setAppId:@"1082428688"];
//            [Appirater setDaysUntilPrompt:0];
//            [Appirater setUsesUntilPrompt:2];
//            [Appirater setSignificantEventsUntilPrompt:-1];
//            [Appirater setTimeBeforeReminding:2];
//            [Appirater setDebug:YES];
//        
         // [Appirater appLaunched:YES];
    }
    
    [GADMobileAds configureWithApplicationID:ADMOB_APP_ID];
    [GADMobileAds configureWithApplicationID:ADMOB_APP_ID_FOR_NATIVE];
    
    
    [iRate sharedInstance].promptAtLaunch = NO;
    return true;
}

//+ (void)initialize
//{
//    [iRate sharedInstance].appStoreID = 1082428688;
//    
//    [iRate sharedInstance].usesUntilPrompt = 1;
//    
//}


- (void)requestFinished:(ASIHTTPRequest *)request
{
    
    NSString *responseString = [request responseString];
    NSDictionary *responseDict = [responseString JSONValue];
    NSInteger isApproved = [[responseDict objectForKey:@"is_approved"] integerValue];
    
    NSLog(@"%@",responseDict);
    NSLog(@"%ld",(long)isApproved);
    
    NSUserDefaults *userDefault   = [NSUserDefaults standardUserDefaults];
    [userDefault setInteger:isApproved forKey:@"isItunesApproved"];
    [userDefault synchronize];
    
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    
    NSError *error = [request error];
    NSLog(@"%@",error.localizedDescription);
}


-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    
    // Resets icon's badge number to zero
    application.applicationIconBadgeNumber = 0;
    
}




- (void)applicationWillResignActive:(UIApplication *)application
{
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
}
- (void)applicationDidBecomeActive:(UIApplication *)application
{
    
  
}

- (void)applicationWillTerminate:(UIApplication *)application
{
}

@end

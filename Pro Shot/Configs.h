//
//  Configs.h
//  Pro Shot
#ifndef Pro_Shot_Configs_h
#define Pro_Shot_Configs_h




// APP INFO ================================
#define APP_NAME @"PhotoEdit"

#define APP_ITUNES_ID @"1185301470"

// Replace "XXXXXXX" with your own App ID, you can find it by clicking on "More -> About This App" button in iTunes Connect, copy the Apple ID and paste it here
#define RATE_US_LINK @"http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=1185301470"

// Replace the link below with the one of your app
#define ITUNES_STORE_LINK @"https://itunes.apple.com/us/app/1185301470"

#define FEEDBACK_EMAIL_ADDRESS @"iftikhersultan@gmail.com"
//-----------------------------------------------------------------------------


// Google admob App id
//Id from Palash's account
//#define ADMOB_APP_ID @"ca-app-pub-4858202199740297~2094029164"
//#define ADMOB_BANNER_ID @"ca-app-pub-4858202199740297/3570762369"
//#define ADMOB_INTERSTITIAl_ID @"ca-app-pub-4858202199740297/6524228768"

#define ADMOB_APP_ID @"ca-app-pub-8340924500267413~5893360689"

#define ADMOB_APP_ID_FOR_NATIVE @"ca-app-pub-8340924500267413~5893360689"

//app id for medium "ca-app-pub-2603477123151360~7962021137"
//ad id for medium ca-app-pub-2603477123151360/1915487535
//
#define ADMOB_BANNER_ID @"ca-app-pub-8340924500267413/8079283084"
#define ADMOB_NATIVE_ID @"ca-app-pub-8340924500267413/3256243330"
#define ADMOB_INTERSTITIAl_ID @"ca-app-pub-8340924500267413/9556016288"

#define TEST_DEVICE_1 @"5982c32c2658c66d48f0f701577a92ae" //Palash's device
#define TEST_DEVICE_2 @"6b5d2277483ae28ec025ac2ca859b825" //prithbiraaj's device
#define TEST_DEVICE_3 @"5982c32c2658c66d48f0f701577a92ae" //User2's device




//#define FB_BANNER_ID @"1317759251627275_1317764994960034"
//#define FB_INTERSTITIAl_ID @"1317759251627275_1317765361626664"


#define FB_BANNER_ID @"421941011523446_421941541523393a"
#define FB_INTERSTITIAl_ID @"421941011523446_421942451523302a"
#define TEST_DEVICE_FOR_FB @"0a6316f63d965e11f832346425d9bd084e0f6654a"

// COLORS ===============================
#define MAIN_COLOR [UIColor colorWithRed:47.0/255.0 green:55.0/255.0 blue:65.0/255.0 alpha:1.0]
#define LIGHT_COLOR [UIColor colorWithRed:246.0/255.0 green:247.0/255.0 blue:251.0/255.0 alpha:1.0]
#define PURPLE_COLOR [UIColor colorWithRed:209.0/255.0 green:118.0/255.0 blue:220.0/255.0 alpha:1.0]
#define MENU_COLOR [UIColor colorWithRed:209.0/255.0 green:211.0/255.0 blue:212.0/255.0 alpha:1.0]

//-----------------------------------------------------------------------------


// FONTS ===============================
#define MAIN_FONT [UIFont fontWithName:@"HelveticaNeue-Thin" size:10]
#define REGULAR_FONT [UIFont fontWithName:@"HelveticaNeue" size:13]

#define NAVBAR_FONT [UIFont fontWithName:@"BebasNeue" size:16]
//-----------------------------------------------------------------------------


// YOUR INSTAGRAM PAGE URL =====================================
#define INSTAGRAM_URL @"http://instagram.com/photolabapp"
//-----------------------------------------------------------------------------


// YOUR INSTAGRAM PAGE URL =====================================
#define FACEBOOK_PAGE_LINK @"https://www.facebook.com/PhotoEdits-336393486831002/"
//-----------------------------------------------------------------------------


// SHARING MESSAGE FOR TWITTER, FACEBOOK AND MAIL ===================
#define SHARING_TITLE @"Hi there!"
#define SHARING_MESSAGE @"Check out my picture, I've made it with #PhotoEdit!"
#define FACEBOOK_LOGIN_ALERT @"Please go to Settings and link your Facebook account to this device!"
#define TWITTER_LOGIN_ALERT @"Please go to Settings and link your Twitter account to this device!"
//-----------------------------------------------------------------------------


// IN-APP PURCHASE PRODUCT ID ========================
#define IAP_PRODUCT @"com.iftikher.photo02.premium"

//#define IAP_PRODUCT_STICKER @"com.rafi.pro.stickers"
//#define IAP_PRODUCT_FRAME @"com.rafi.pro.frames"
//#define IAP_PRODUCT_FONT @"com.rafi.pro.fonts"
//#define IAP_PRODUCT_EFFECT @"com.rafi.pro.effects"
//
//#define IAP_PRODUCT_REMOVE_AD @"com.rafi.pro.removeads"

//-----------------------------------------------------------------------------

#define shouldShowFBAdd NO
#define freeDaysForRating 3

// FREE ITEMS AVAILABLE WITH NO IAP ================
#define freeStickers 9     +1
#define freeFrames   9      +1
#define freeFonts   9      +1
#define freeFilters   9      +1
#define freeEffects   9      +1
#define freeBorders  9      +1
#define freeTextures 9      +1
//-----------------------------------------------------------------------------

// USE COUNTS BEFORE RATING ================
#define freeColorUsage 3
#define freeEffectUsage  3
#define freeFilterUsage  3
#define freeTextUsage 3
//-----------------------------------------------------------------------------



#endif

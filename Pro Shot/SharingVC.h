/*============================
 
 Pro Shot
 
 iOS 7/8 iPhone Photo Editor App template
 created by FV iMAGINATION - 2014
 http://www.fvimagination.com
 
 ==============================*/


#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "ALAssetsLibrary+CustomPhotoAlbum.h"
#import <Social/Social.h>

#import <GoogleMobileAds/GoogleMobileAds.h>

// Photo library
ALAssetsLibrary *library;

// Document Interaction for dharing options
UIDocumentInteractionController *docInteraction;
// Social controller
SLComposeViewController *Socialcontroller;

// RestClient for Dropbox and Views for its layout
UIActivityIndicatorView *indicatorView;

// image that needs to be shared/saved
UIImage *imageToBeShared;

BOOL isAdRemoved;


@interface SharingVC : UIViewController
<
UIDocumentInteractionControllerDelegate,
MFMailComposeViewControllerDelegate,
GADInterstitialDelegate,
UIAlertViewDelegate
>

// Labels
@property (weak, nonatomic) IBOutlet UILabel *sharingOptionsLabel;
@property (weak, nonatomic) IBOutlet UILabel *photoLibraryLabel;
@property (weak, nonatomic) IBOutlet UILabel *mailLabel;

@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;



// Preview Image
@property (weak, nonatomic) IBOutlet UIImageView *previewImage;

// Container ScrollView
@property (strong, nonatomic) IBOutlet UIScrollView *containerScrollView;
@property (nonatomic, strong) GADInterstitial *interstitial;



@end

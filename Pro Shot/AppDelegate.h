/*============================
 
 Pro Shot
 
 iOS 7/8 iPhone Photo Editor App template
 created by FV iMAGINATION - 2014
 http://www.fvimagination.com
 
 ==============================*/


#import <UIKit/UIKit.h>

@class IntroScreen, InfoScreen;


@interface AppDelegate : UIResponder <UIApplicationDelegate>


@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) IntroScreen *introScreen;
@property (strong, nonatomic) InfoScreen *infoScreen;
@property (nonatomic, assign) NSUInteger appStoreID;
@property (nonatomic, assign) float daysUntilPrompt;
@property (nonatomic, assign) NSUInteger usesUntilPrompt;



@end

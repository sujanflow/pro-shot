/*============================
 
 Pro Shot
 
 iOS 7/8 iPhone Photo Editor App template
 created by FV iMAGINATION - 2014
 http://www.fvimagination.com
 
 ==============================*/


#import "SharingVC.h"
#import <Social/Social.h>
#import "SettingsVC.h"
#import "Configs.h"


@interface SharingVC ()
{
    BOOL isShareSuccessful;
}

@end


@implementation SharingVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(BOOL)prefersStatusBarHidden {
    return true;
}
// Prevent the StatusBar from showing up after picking an image
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    [[UIApplication sharedApplication] setStatusBarHidden:true];
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    
    isShareSuccessful=NO;
    // Gets the image from previous MainScreen =======
    NSLog(@"imageToBeShared %@",imageToBeShared);
    _previewImage.image = imageToBeShared;

    // Init the device Assets Library
    library = [[ALAssetsLibrary alloc] init];

    
    // Setup Localized text
    _photoLibraryLabel.text = NSLocalizedString(@"Photo Library", @"");
    _sharingOptionsLabel.text = NSLocalizedString(@"Share Your Picture", @"");
    _mailLabel.text = NSLocalizedString(@"Mail", @"");
    
    // Resize container scrollView
    _containerScrollView.contentSize = CGSizeMake(_containerScrollView.frame.size.width, 578);
    
    
    isAdRemoved=[[NSUserDefaults standardUserDefaults] boolForKey:@"isAdRemoved"];
    
    
    if(!isAdRemoved)
    {
        self.interstitial = [self createAndLoadInterstitial];

    }
    //for iphone x
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone) {
        
        switch ((int)[[UIScreen mainScreen] nativeBounds].size.height) {
                
            case 1792:
            case 2436:
            case 2688:

                printf("iPhone X");
               self.titleLabel.center = CGPointMake(self.titleLabel.center.x, self.titleLabel.center.y+30);
                self.cancelButton.center = CGPointMake(self.cancelButton.center.x, self.cancelButton.center.y+30);
                self.previewImage.center = CGPointMake(self.previewImage.center.x, self.previewImage.center.y+30);
                self.sharingOptionsLabel.center = CGPointMake(self.sharingOptionsLabel.center.x, self.sharingOptionsLabel.center.y+30);
                break;
            default:
                printf("unknown");
                
        }
    }
    
}

-(void)viewDidAppear:(BOOL)animated
{
    NSLog(@"appear");
   
}

-(void)viewDidDisappear:(BOOL)animated {
    library = nil;
}

- (void)appDidBecomeActive:(NSNotification *)notification {
    NSLog(@"did become active notification");
    
    if (isShareSuccessful) {
        [self showInterstitial];
        
    }
}

-(void) showInterstitial
{
    if (self.interstitial.isReady) {
        [self.interstitial presentFromRootViewController:self];
    } else {
        NSLog(@"Ad wasn't ready");
    }
    isShareSuccessful=NO;
}



#pragma mark - CANCEL BUTTON ============================
- (IBAction)cancelButt:(id)sender {
    
    NSLog(@"cancelButtcancelButtcancelButtcancelButt");
    
    [self dismissViewControllerAnimated:true completion:nil];
}




#pragma mark - SAVE IMAGE TO PHOTO LIBRARY ========================
- (IBAction)photoLibButt:(id)sender {
    
    // Save to Photo Library
    if (!saveToCustomAlbum) {
    UIImageWriteToSavedPhotosAlbum(imageToBeShared, nil, nil, nil);
    
    isShareSuccessful=YES;
    
    UIAlertView *myAlert = [[UIAlertView alloc]initWithTitle: APP_NAME
    message: NSLocalizedString(@"Your Picture has been saved into Photo Library!", @"")
    delegate: self
    cancelButtonTitle: NSLocalizedString(@"OK", @"")
    otherButtonTitles:nil];
    [myAlert show];
    
    
    // Save Photo to Custom album
    } else {
        [library saveImage:imageToBeShared toAlbum: APP_NAME withCompletionBlock:^(NSError *error) {
            if (error != nil) {
                isShareSuccessful=NO;
                NSLog(@"Error: %@", [error description]);
        } else {
            NSString *string1  = NSLocalizedString(@"You picture has been saved into ", @"");
            NSString *string2 = NSLocalizedString(@" Album", @"");
            
            isShareSuccessful = YES;
            
            UIAlertView *myAlert = [[UIAlertView alloc]initWithTitle: APP_NAME
            message: [NSString stringWithFormat:@"%@ %@ %@", string1, APP_NAME, string2]
            delegate: self
            cancelButtonTitle: NSLocalizedString(@"OK", @"")
            otherButtonTitles:nil];
            [myAlert show];
        }
    }];
        
    }
    
}





#pragma mark - FACEBOOK SHARING ========================
- (IBAction)facebookButt:(id)sender {
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        Socialcontroller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [Socialcontroller setInitialText: NSLocalizedString(SHARING_MESSAGE, @"")];
        [Socialcontroller addImage: imageToBeShared];
        [self presentViewController:Socialcontroller animated:true completion:nil];
        
    } else {
        isShareSuccessful=NO;
        
        
        NSString *message = NSLocalizedString(FACEBOOK_LOGIN_ALERT, @"");
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle: APP_NAME
        message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
    
    [Socialcontroller setCompletionHandler:^(SLComposeViewControllerResult result) {
        NSString *output;
        switch (result) {
            case SLComposeViewControllerResultCancelled:
                isShareSuccessful=NO;
                
                output = NSLocalizedString(@"Sharing Cancelled!", @"");
                break;
            case SLComposeViewControllerResultDone:
                isShareSuccessful=YES;
                
                output = NSLocalizedString(@"Your picture is on Facebook!", @"");
                break;
                
            default: break;
        }
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook"
        message:output delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
    }];
}





#pragma mark - TWITTER SHARING ========================
- (IBAction)twitterButt:(id)sender {
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        Socialcontroller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [Socialcontroller setInitialText: NSLocalizedString(SHARING_MESSAGE, @"")];
        [Socialcontroller addImage: imageToBeShared];
        [self presentViewController:Socialcontroller animated:true completion:nil];
        
    } else {
        isShareSuccessful=NO;
        
        NSString *message = NSLocalizedString(TWITTER_LOGIN_ALERT, @"");
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle: APP_NAME
        message:message delegate:nil
        cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
    
    [Socialcontroller setCompletionHandler:^(SLComposeViewControllerResult result) {
        NSString *output;
        switch (result) {
            case SLComposeViewControllerResultCancelled:
                isShareSuccessful=NO;
                
                output = NSLocalizedString(@"Sharing Cancelled!", @"");
                break;
            case SLComposeViewControllerResultDone:
                isShareSuccessful=YES;
                
                output = NSLocalizedString(@"Your picture is on Twitter!", @"");
                break;
                
            default: break;
        }
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Twitter"
        message:output delegate:self
        cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];

    }];
}



#pragma mark - EMAIL SHARING ==============================
- (IBAction)mailButt:(id)sender {
    NSString *emailTitle = NSLocalizedString(SHARING_TITLE, @"");
    NSString *messageBody = NSLocalizedString(SHARING_MESSAGE, @"");
    
    // Allocs the Mail composer controller
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML: true];
    
    // Prepares the image to be shared by Email
    NSData *imageData = UIImagePNGRepresentation(imageToBeShared);
    [mc addAttachmentData:imageData  mimeType:@"image/png" fileName:@"myImage.png"];
    
    if ([MFMailComposeViewController canSendMail]) {
        NSLog(@"can send mail");
    
        [self presentViewController:mc animated:true completion: nil];
        
      }else
      {
           NSLog(@"can not send mail");
           UIAlertView *av = [[UIAlertView alloc] initWithTitle: APP_NAME
                                                 message:NSLocalizedString(@"Please configure your mail account in device settings.",@"")
                                                delegate:nil
                                       cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                       otherButtonTitles:nil];
           [av show];
      }

    
    
}

// Email delegates ================
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)results error:(NSError *)error  {
    
    
    switch (results) {
        case MFMailComposeResultCancelled: {
            
            isShareSuccessful=NO;
            
            UIAlertView *myAlert = [[UIAlertView alloc]initWithTitle: NSLocalizedString(@"Email Cancelled!", @"")
            message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [myAlert show];
        }
            break;
            
        case MFMailComposeResultSaved:{
            isShareSuccessful=YES;
            
            UIAlertView *myAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Email Saved!", @"")
            message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [myAlert show];
        }
            break;
            
        case MFMailComposeResultSent:{
            isShareSuccessful=YES;
            
            UIAlertView *myAlert = [[UIAlertView alloc]initWithTitle: NSLocalizedString(@"Email Sent!", @"")
            message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [myAlert show];
        }
            break;
            
        case MFMailComposeResultFailed:{
            
            isShareSuccessful=NO;
            
            UIAlertView *myAlert = [[UIAlertView alloc]initWithTitle: NSLocalizedString(@"Email error, try again!", @"")
                message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [myAlert show];
        }
            break;
            
            
        default: break;
    }
    // Dismiss the Email View Controller
    [self dismissViewControllerAnimated:true completion: nil];
}




#pragma mark - INSTAGRAM SHARING =====================
- (IBAction)instagramButt:(id)sender {
    /* =================
     NOTE: The following methods work only on real device, not iOS Simulator, and you should have Instagram already installed into your device!
     ================= */
    
    NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
    if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
        
        
        //Save the edited Image to directory
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, true);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:@"savedImage.jpg"];
        UIImage *image = imageToBeShared;
        NSData *imageData = UIImagePNGRepresentation(image);
        [imageData writeToFile:savedImagePath atomically:false];
        
        //Load the edited Image
        NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:@"savedImage.jpg"];
        UIImage *tempImage = [UIImage imageWithContentsOfFile:getImagePath];
        
        //Hook the edited Image with Instagram
        NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/Image.igo"];
        [UIImageJPEGRepresentation(tempImage, 1.0) writeToFile:jpgPath atomically:true];
        
        // Prepare the DocumentInteraction with the .igo image for Instagram
        NSURL *instagramImageURL = [[NSURL alloc] initFileURLWithPath:jpgPath];
        docInteraction = [UIDocumentInteractionController interactionControllerWithURL:instagramImageURL];
        [docInteraction setUTI:@"com.instagram.exclusivegram"];
        docInteraction.delegate = self;
        
        // Open the DocumentInteraction Menu
        [docInteraction presentOpenInMenuFromRect:CGRectZero inView: self.view animated:true];
        
    } else {
    // Open an AlertView as sharing result when the Document Interaction Controller gets dismissed
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: NSLocalizedString(@"Instagram not found", @"")
    message: NSLocalizedString(@"Please install Instagram on your device!", @"") delegate:nil
    cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    }
}




#pragma mark - PIQUK SHARING =====================
- (IBAction)piqukButt:(UIButton*)sender {
        
    NSMutableArray *sharingItems = [NSMutableArray new];
    
    [sharingItems addObject:imageToBeShared];
    
    UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    
    controller.popoverPresentationController.sourceView = self.view;
    CGRect temp = [UIScreen mainScreen].bounds;
    temp.size.height = temp.size.height/2;
    controller.popoverPresentationController.sourceRect = temp;
    [self presentViewController:controller animated:YES completion:nil];

}




#pragma mark - SEND IMAGE VIA WHATSAPP =====================
- (IBAction)whatsappButt:(id)sender {
    if ([[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"whatsapp://app"]]) {
        
        
    NSString *savePath  = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/whatsAppTmp.wai"];
            
    [UIImageJPEGRepresentation(imageToBeShared, 1.0) writeToFile:savePath atomically:true];
            
    docInteraction = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:savePath]];
    docInteraction.UTI = @"net.whatsapp.image";
    docInteraction.delegate = self;
        
    [docInteraction presentOpenInMenuFromRect:CGRectMake(0, 0, 0, 0) inView:self.view animated: true];
        
    } else {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle: NSLocalizedString(@"WhatsApp not found", @"")
        message: NSLocalizedString(@"Please install WhatsApp on your device!", @"") delegate:self
        cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }

}

#pragma mark - UIDocumentInteractionDelegate
- (void)documentInteractionController:(UIDocumentInteractionController *)controller
        willBeginSendingToApplication:(NSString *)application
{
    NSLog(@"willBeginSendingToApplication %@",application);
    isShareSuccessful=YES;
    
  //  [self showInterstitial];
    
    
}

- (void)documentInteractionController:(UIDocumentInteractionController *)controller
           didEndSendingToApplication:(NSString *)application
{
   // NSLog(@"didEndSendingToApplication %@",application);

}

#pragma mark - GADInterstitialViewDelegate

- (GADInterstitial *)createAndLoadInterstitial {
    
    GADInterstitial *interstitial =
    [[GADInterstitial alloc] initWithAdUnitID:ADMOB_INTERSTITIAl_ID];
    interstitial.delegate = self;
    
    GADRequest *request = [GADRequest request];
    request.testDevices = @[ kGADSimulatorID, TEST_DEVICE_1,TEST_DEVICE_2,TEST_DEVICE_3 ];
    [interstitial loadRequest:request];
    //[interstitial loadRequest:[GADRequest request]];
    
    return interstitial;
    
}

- (void)interstitialDidDismissScreen:(GADInterstitial *)interstitial {
    self.interstitial = [self createAndLoadInterstitial];
    NSLog(@"interstitialDidDismissScreen");
    
    if([SKStoreReviewController class]){
        [SKStoreReviewController requestReview] ;
    }
    
}

#pragma mark - AlertViewDelegate

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (isShareSuccessful) {
        
        [self showInterstitial];
    } else {
        NSLog(@"Share not successful");
    }
    
    isShareSuccessful=NO;

}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
